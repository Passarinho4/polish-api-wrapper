package com.tegess

import java.net.URI
import java.nio.charset.StandardCharsets
import java.security.Security

import com.google.gson.Gson
import com.squareup.okhttp.{Interceptor, Response}
import org.apache.commons.io.IOUtils
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.scheduling.annotation.EnableScheduling
import pl.kir.psd2.tpp.conn.api.ScopeDetailsHelper
import pl.kir.psd2.tpp.conn.crypto.{BodySigner, KeyPair}
import pl.kir.psd2.tpp.conn.internal.GsonConfig
import pl.kir.psd2.tpp.polishapi.ApiClient
import com.avsystem.commons.jiop.JavaInterop._
import com.tegess.enablebanking.EnableBankingService
import org.bouncycastle.jce.provider.BouncyCastleProvider
import org.springframework.context.annotation.Bean
import pl.kir.psd2.tpp.conn.RawResponseInterceptor
import pl.kir.psd2.tpp.polishapi.model.PrivilegeDomesticTransfer

@SpringBootApplication
@EnableScheduling
class Config {

  def gson(): Gson = GsonConfig.get()

  @Bean def aliorService() = new EnableBankingService

  @Bean
  def bodySigner(): BodySigner = new BodySigner(Config.qSealCKeyPair(), new URI(Config.publicKeyUrl), gson())

  @Bean
  def apiClient(): ApiClient = {
    val apiClient = new ApiClient()

    apiClient.setBasePath(Config.serverUrl)
    apiClient.setDebugging(true)
    apiClient.getJSON.setGson(gson())
    apiClient.getHttpClient.interceptors().add(new RawResponseInterceptor())
    if (apiClient.getBasePath.startsWith("https")) {
      apiClient.setSslCaCert(null)
      apiClient.setKeyManagers(Config.qwacKeyPair().createKeyManager())
      apiClient.setSslCaCert(classOf[Config].getResourceAsStream("/my_certs/HUB_PSD2_TEST_SUB_CA1.chain.cer"))
    }
    apiClient.setConnectTimeout(6000)
    apiClient.setReadTimeout(6000)
    apiClient.setWriteTimeout(6000)

    val headers = Map("X-ASPSP-NAME" -> "Mock TPP CA1 sp. z o.o.")
    for ((header, value) <- headers) {
      apiClient.getHttpClient.networkInterceptors().add((chain: Interceptor.Chain) => chain.proceed(chain.request().newBuilder()
        .addHeader(header, value).build()));
    }

    apiClient
  }

  val json = """
        {"privilegeList":[
          {"pis:domestic":{
            "scopeUsageLimit":"single",
            "recipient":{
              "accountNumber":"06551905220000000000023717",
              "nameAddress":{"value":["Bartosz Banasik","Wscieklych Wezy 5/10","Arlamow, 00-007","Polska"]}
            },
            "sender":{
              "accountNumber":"35551905220000000000019315",
              "nameAddress":{"value":["Grzegorz Brzeczyszczykiewicz","Pomaranczowych domow 0/1","Mediolanek, 01-234","Polska"]}
            },
            "transferData":{"description":"DOMESTIC transfer","amount":"100.00","executionDate":"%LocalDate%","currency":"PLN"},
            "deliveryMode":"StandardD1",
            "system":"Elixir",
            "executionMode":"Immediate"
          }}
        ]}
        """

}

object Config {
  val qwac = "/my_certs/QWAC_MS.pem"
  val qsealc = "/my_certs/QSEALC_MS.pem"
  val privKey = "/my_certs/QWAC_MS.ppk"
  val publicKeyUrl = "http://publo.app/QSEALC_MS.pem"
  val serverUrl = "https://api-obh.kir.pl"

  def qwacKeyPair(): KeyPair = {
    KeyPair.create(
      IOUtils.toString(classOf[Config].getResourceAsStream(qwac), StandardCharsets.UTF_8),
      IOUtils.toString(classOf[Config].getResourceAsStream(privKey), StandardCharsets.UTF_8))
  }

  def qSealCKeyPair(): KeyPair = {
    KeyPair.create(
      IOUtils.toString(classOf[Config].getResourceAsStream(qsealc), StandardCharsets.UTF_8),
      IOUtils.toString(classOf[Config].getResourceAsStream(privKey), StandardCharsets.UTF_8))
  }
}

object Main extends App {
  Security.addProvider(new BouncyCastleProvider())
  System.setProperty( "javax.net.ssl.trustStore", "/resources/comp.jks" )
  SpringApplication.run(classOf[Config])

}

case class ApiClientWrapper(apiClient: ApiClient, sdh: ScopeDetailsHelper, pdt: PrivilegeDomesticTransfer)
