package com.tegess.api

import java.net.InetAddress
import java.nio.charset.StandardCharsets
import java.util.UUID
import java.util.regex.Pattern

import com.avsystem.commons.jiop.JavaInterop._
import com.tegess.{ApiClientWrapper, Config}
import javax.servlet.http.HttpServletResponse
import org.apache.commons.lang3.RandomStringUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.{RequestMapping, RequestParam, RestController}
import pl.kir.psd2.tpp.conn.RawResponseInterceptor
import pl.kir.psd2.tpp.conn.api.ScopeDetailsHelper
import pl.kir.psd2.tpp.conn.crypto.{BodySigner, SignatureVerifier}
import pl.kir.psd2.tpp.polishapi.api.{AsApi, CafApi, PisApi}
import pl.kir.psd2.tpp.polishapi.model._
import pl.kir.psd2.tpp.polishapi.{ApiClient, ApiResponse}

@RestController
class Api @Autowired()(
                        apiClient: ApiClient,
                        bodySigner: BodySigner,

                      ) {

  private val cache = new JHashMap[String, ApiClientWrapper]()
  private val userCache = new JHashMap[String, Wrapper]()

  private def createWrapper(fromAccount: String, toAccount: String, amount: String, title: String): ApiClientWrapper = {
    val fields = Map("redirectUri" -> "https://bank-obh.kir.pl/#/login/",
      "userAgent" -> "Swagger-Codegen/1.0.0/java")

    val sdh = new ScopeDetailsHelper
    sdh.setTppId(fields.getOrElse("tppId",
      Option(Config.qSealCKeyPair()).map(_.getTppIdFromX509).getOrElse(Config.qwacKeyPair().getTppIdFromX509)))

    sdh.setRedirectUrl(fields("redirectUri"))

    sdh.instantiateConsentFromJSON("PIS", 180, Api.prepareJson(fromAccount, toAccount, amount, title))

    val pisDomestic = sdh.getPrivilegeList.asScala.map(r => Option(r.getPisdomestic)).find(_.isDefined).flatten
    val privilegeDomesticTransfer = pisDomestic.get

    ApiClientWrapper(apiClient, sdh, privilegeDomesticTransfer)
  }

  private def confirmationOfFunds(apiClientWrapper: ApiClientWrapper) = {
    val client = apiClientWrapper.apiClient
    val sdh = apiClientWrapper.sdh
    val pdt = apiClientWrapper.pdt
    val request = new ConfirmationOfFundsRequest
    request.setRequestHeader(sdh.getRequestHeaderWithoutToken)
    request.accountNumber(pdt.getSender.getAccountNumber)
    request.amount(pdt.getTransferData.getAmount)
    request.currency(pdt.getTransferData.getCurrency)

    val api = new CafApi(client)
    val response = api.getConfirmationOfFundsWithHttpInfo(
      "deflate",
      "en-gb",
      StandardCharsets.UTF_8.displayName(),
      bodySigner.sign(request),
      request.getRequestHeader.getRequestId.toString,
      request)

    if (response.getStatusCode != 200) {
      throw new Exception("Metoda caf:confirmationOfFunds odpowiedziała kodem: " + response.getStatusCode())
    }
    Api.checkJWSSignature(response, apiClientWrapper)

    response.getData.isFundsAvailable

  }

  private def executeAsAuthorize(apiClientWrapper: ApiClientWrapper): (String, String) = {
    val client = apiClientWrapper.apiClient
    val sdh = apiClientWrapper.sdh
    val request = new AuthorizeRequest
    request.setRequestHeader(sdh.getRequestHeaderWithoutTokenAS)
    request.setResponseType("code")
    request.clientId(sdh.getTppId)
    request.setRedirectUri("http://104.40.209.110/authenticated")
    request.setScope(sdh.getScopeType)
    val state = UUID.randomUUID.toString
    request.setState(state)
    request.setScopeDetails(sdh.getScopeDetailsInput)
    val api = new AsApi(client)
    val response = api.authorizeWithHttpInfo(
      "deflate",
      "en-gb",
      StandardCharsets.UTF_8.displayName(),
      bodySigner.sign(request),
      request.getRequestHeader.getRequestId.toString,
      request
    )
    if (response.getStatusCode() != 200)
      throw new Exception("Metoda as:authorize odpowiedziała kodem: " + response.getStatusCode());

    Api.checkJWSSignature(response, apiClientWrapper)

    val uri = response.getData.getAspspRedirectUri
    if (uri == null || uri.equals(""))
      throw new Exception("no consent url send in Authorize response")

    (uri, state)
  }

  private def extractRequestId(apiClientWrapper: ApiClientWrapper, uri: String) = {
    var redirectDomain = apiClientWrapper.sdh.getRedirectUrl
    if (redirectDomain.length() > 9) {
      redirectDomain = uri.substring(0, uri.indexOf("/", 9))
    }

    val tokenPattern = "login/(.*)"
    val r = Pattern.compile(tokenPattern);
    val m = r.matcher(uri);
    if (!m.find()) throw new RuntimeException("Fetch Login Token with regex: " + tokenPattern + " - no match");
    val authRequestId = m.group(1);

    if (authRequestId.equals("")) throw new Exception("Brak tokenu zwróconego przez metodę Authorize");
    authRequestId
  }

  @RequestMapping(path = Array("test"))
  def test(
            @RequestParam("fromAccount") fromAccount: String,
            @RequestParam("fromUser") fromUser: String,
            @RequestParam("toAccount") toAccount: String,
            @RequestParam("toUser") toUser: String,
            @RequestParam("amount") amount: String,
            @RequestParam("title") title: String
          ) = {
    val apiClientWrapper = createWrapper(fromAccount, toAccount, amount, title)
    val hasEnoughMoney = confirmationOfFunds(apiClientWrapper)
    if (!hasEnoughMoney) {
      throw new Exception(s"On account ${fromAccount} there is not enough money.")
    }
    val (uri, state) = executeAsAuthorize(apiClientWrapper)
    val requestId = extractRequestId(apiClientWrapper, uri)
    this.cache.put(state, apiClientWrapper)
    userCache.put(state, Wrapper(fromUser, toUser, amount))
    apiClientWrapper.sdh.getRedirectUrl + requestId
  }

  private def getAccessToken(token: String, wrapper: ApiClientWrapper) = {
    val request = new TokenRequest
    request.setRequestHeader(wrapper.sdh.getRequestHeaderWithoutTokenAS)
    request.setGrantType("authorization_code")
    request.setCode(token)
    request.setRedirectUri(wrapper.sdh.getRedirectUrl)
    request.setClientId(wrapper.sdh.getTppId)
    request.setScope(wrapper.sdh.getScopeType)
    request.setScopeDetails(wrapper.sdh.getScopeDetailsInput)
    request.setIsUserSession(true)
    request.setUserIp(InetAddress.getLocalHost.getHostAddress)

    val api = new AsApi(wrapper.apiClient)
    val response = api.tokenWithHttpInfo(
      "deflate",
      "en-gb",
      StandardCharsets.UTF_8.displayName(),
      bodySigner.sign(request),
      request.getRequestHeader.getRequestId.toString,
      request
    )

    if (response.getStatusCode != 200)
      throw new Exception("Metoda as:token odpowiedziała kodem: " + response.getStatusCode)

    Api.checkJWSSignature(response, wrapper)
    val accessToken = response.getData.getAccessToken
    wrapper.sdh.setAccessToken(accessToken)
    accessToken
  }

  private def createPayment(wrapper: ApiClientWrapper) = {
    val request = new PaymentDomesticRequest()
    request.setRequestHeader(wrapper.sdh.getRequestHeaderCallback)
    request.setRecipient(wrapper.pdt.getRecipient)
    request.setSender(wrapper.pdt.getSender)
    request.setTransferData(wrapper.pdt.getTransferData)
    val tppTransactionId = RandomStringUtils.random(12, true, true)
    request.setTppTransactionId(tppTransactionId)
    request.setDeliveryMode(PaymentDomesticRequest.DeliveryModeEnum.fromValue(wrapper.pdt.getDeliveryMode.getValue))
    request.setSystem(PaymentDomesticRequest.SystemEnum.fromValue(wrapper.pdt.getSystem.getValue))
    request.setHold(false)
    request.setExecutionMode(PaymentDomesticRequest.ExecutionModeEnum.fromValue(wrapper.pdt.getExecutionMode.getValue))

    val api = new PisApi(wrapper.apiClient)
    val response = api.domesticWithHttpInfo(
      "Bearer " + wrapper.sdh.getAccessToken,
      "deflate",
      "en-gb",
      StandardCharsets.UTF_8.displayName(),
      bodySigner.sign(wrapper.sdh),
      request.getRequestHeader.getRequestId.toString,
      request
    )

    if (response.getStatusCode != 200)
      throw new Exception("Metoda pis:domestic odpowiedziała kodem: " + response.getStatusCode)

    Api.checkJWSSignature(response, wrapper)
    val paymentId = response.getData.getPaymentId
    (paymentId, tppTransactionId)
  }

  def checkPaymentStatus(paymentId: String, wrapper: ApiClientWrapper, tppTransactionId: String) = {
    val request = new PaymentRequest()
    request.setRequestHeader(wrapper.sdh.getRequestHeader)
    request.setPaymentId(paymentId)
    request.setTppTransactionId(tppTransactionId)

    val api = new PisApi(apiClient)
    val response = api.getPaymentWithHttpInfo(
      "Bearer " + wrapper.sdh.getAccessToken,
      "deflate",
      "en-gb",
      StandardCharsets.UTF_8.displayName(),
      bodySigner.sign(request),
      request.getRequestHeader.getRequestId.toString,
      request
    )
    if (response.getStatusCode != 200)
      throw new Exception("Metoda pis:getPayment odpowiedziała kodem: " + response.getStatusCode)

    Api.checkJWSSignature(response, wrapper)
  }

  @RequestMapping(path = Array("authenticated"))
  def authenticated(
                     @RequestParam("code") code: String,
                     @RequestParam("state") state: String,
                     response: HttpServletResponse
                   ) = {

    val token = code
    val wrapper = cache.get(state)
    val accessToken: String = getAccessToken(token, wrapper)
    println("ACCESS_TOKEN = " + accessToken)
    val (paymentId, transactionId) = this.createPayment(wrapper)
    paymentId
    //val accessToken2 = getAccessToken(token, wrapper)
    //this.checkPaymentStatus(paymentId, wrapper, transactionId)
    val wrapper1 = userCache.get(state)
    response.sendRedirect(s"https://publo.app/notify_user?fromUser=${wrapper1.fromUser}&toUser=${wrapper1.toUser}&amount=${wrapper1.amount}")

  }

}

object Api {

  val signatureVerifier = new SignatureVerifier

  def checkJWSSignature(response: ApiResponse[_], clientWrapper: ApiClientWrapper) = {
    val jws = response.getHeaders.get("X-JWS-SIGNATURE")
    if (jws == null || jws.isEmpty) {
      throw new Exception("No JWS Signature")
    }
    var rawResponse = ""
    for (interceptor <- clientWrapper.apiClient.getHttpClient.interceptors().asScala) {
      interceptor match {
        case i: RawResponseInterceptor => rawResponse = i.lastResponse
        case _ =>
      }
    }

    println("Sprawdzam poprawnosc odpowiedzi - sygnatura JWS:")
    println(jws.get(0))
    println("Niezmodyfikowana odpowiedz:")
    println(rawResponse)
    println("Wynik" + signatureVerifier.verifySignature(jws.get(0), rawResponse))

    if (!signatureVerifier.verifySignature(jws.get(0), rawResponse)) {
      throw new Exception("Verified JWS signature is incorrect")
    }

  }

  def prepareJson(fromAccount: String, toAccount: String, amount: String, title: String): String =
    s"""
        {"privilegeList":[
          {"pis:domestic":{
            "scopeUsageLimit":"single",
            "recipient":{
              "accountNumber":"${toAccount}",
              "nameAddress":{"value":["Bartosz Banasik","Wscieklych Wezy 5/10","Arlamow, 00-007","Polska"]}
            },
            "sender":{
              "accountNumber":"${fromAccount}",
              "nameAddress":{"value":["Grzegorz Brzeczyszczykiewicz","Pomaranczowych domow 0/1","Mediolanek, 01-234","Polska"]}
            },
            "transferData":{"description":"${title}","amount":"${amount}","executionDate":"%LocalDate%","currency":"PLN"},
            "deliveryMode":"StandardD1",
            "system":"Elixir",
            "executionMode":"Immediate"
          }}
        ]}
        """
}


case class Wrapper(fromUser: String, toUser: String, amount: String)
