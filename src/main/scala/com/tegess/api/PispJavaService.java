package com.tegess.api;

import com.enablebanking.ApiClient;
import com.enablebanking.api.PispApi;
import com.enablebanking.model.*;
import com.tegess.enablebanking.EnableWrapper;
import com.tegess.enablebanking.ResultWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

//import static com.eb.demo.DemoUtils.blockReadRedirectedUrl;
//import static com.eb.demo.DemoUtils.parseQueryParams;

public class PispJavaService {

    private static Map<String, EnableWrapper> cache2 = new HashMap<>();

    public static ResultWrapper doPayment(String from, String to, String amount, String title) {
        //System.setProperty( "javax.net.ssl.trustStore", "/resources/comp.jks" );

        String paymentAuthRedirectUri = "https://publo.app/alior"; // !!! PUT YOUR REDIRECT URI HERE

        String state = UUID.randomUUID().toString();
        ConnectorSettings settings = new AliorConnectorSettings() // one might choose another bank here
                .clientId("08565454-4686-4581-871f-ca3a0b7cc5af")  // API client ID
                .clientSecret("xU1aC1sS8qH2iA0dL6hU8tM8nY7dE8wP8pT8gI2gR5sY0rG3uD")
                //.certPath("cert-path")  // Path or URI QWAC certificate in PEM format
                //.keyPath("key-path")  // Path or URI to QWAC certificate private key in PEM format
                //.signKeyPath("sign-key-path")  // Path or URI to QSeal certificate in PEM format
                //.signPubKeySerial("sign-pub-key-serial")  // Public serial key of the QSeal certificate located in signKeyPath
                //.signFingerprint("sign-fingerprint")
                //.signCertUrl("sign-cert-url")
                .paymentAuthRedirectUri(paymentAuthRedirectUri)  // URI where clients are redirected to after payment authorization.
                .paymentAuthState(state)  // This value returned to paymentAuthRedirectUri after payment authorization.
                .sandbox(true);

        ApiClient apiClient = new ApiClient(settings);

        PispApi pispApi = new PispApi(apiClient);
        UnstructuredRemittanceInformation remittance = new UnstructuredRemittanceInformation();
        remittance.add(title);

        PaymentRequestResource prr = new PaymentRequestResource()
                .creditTransferTransaction(Arrays.asList(
                        new CreditTransferTransaction()
                                .instructedAmount(new AmountType()
                                        .currency("EUR")
                                        .amount(new BigDecimal(amount)))
                                .frequency(FrequencyCode.DAIL)
                                .beneficiary(
                                        new Beneficiary()
                                                .creditor(
                                                        new PartyIdentification()
                                                                .name("Creditor name")
                                                                .postalAddress(
                                                                        new PostalAddress()
                                                                                .addressLine(Arrays.asList(
                                                                                        "Creditor Name ",
                                                                                        "Creditor Address 1",
                                                                                        "Creditor Address 2"))
                                                                                .country("RO")))
                                                .creditorAccount(new AccountIdentification()
                                                        .iban(to)))
                                .remittanceInformation(remittance)))
                .paymentTypeInformation(new PaymentTypeInformation()
                        .serviceLevel(ServiceLevelCode.SEPA) // will be resolved to "pis:EEA"
                        .localInstrument("SEPA")) // set explicitly, can also be for example SWIFT or ELIXIR
                .debtor(new PartyIdentification()
                        .name("Debtor name")
                        .postalAddress(new PostalAddress()
                                .addressLine(Arrays.asList(
                                        "Debtor Name",
                                        "Debtor Address 1",
                                        "Debtor Address 2"))
                                .country("PL")))
                .debtorAccount(new AccountIdentification().iban(from));
        HalPaymentRequestCreation c = pispApi.makePaymentRequest(prr);
        cache2.put(state, new EnableWrapper(pispApi, prr, c));
        return new ResultWrapper(c.getLinks().getConsentApproval().getHref(), state);

        //String redirectedUrl = blockReadRedirectedUrl(c.getLinks().getConsentApproval().getHref(), authRedirectUri);
        //return "xd";

//        // calling helper functions for CLI interaction
//
//
//        Map<String, String> parsedQueryParams = parseQueryParams(redirectedUrl);
//        HalPaymentRequest pr = pispApi.makePaymentRequestConfirmation(
//                c.getPaymentRequestResourceId(),
//                new PaymentRequestConfirmation().psuAuthenticationFactor(parsedQueryParams.get("code")).paymentRequest(prr));

        //log.info("Payment request: {}", pr);
    }

    public static String check(String code, String state) {
        EnableWrapper wrapper = PispJavaService.cache2.get(state);
        HalPaymentRequest pr = wrapper.api().makePaymentRequestConfirmation(
                wrapper.creation().getPaymentRequestResourceId(),
                new PaymentRequestConfirmation().psuAuthenticationFactor(code).paymentRequest(wrapper.prr()));
        System.out.println(pr);
        return "xd";
    }
}
