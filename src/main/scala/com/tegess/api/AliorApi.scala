package com.tegess.api

import com.tegess.enablebanking.EnableBankingService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.{RequestMapping, RequestParam, RestController}

@RestController
class AliorApi @Autowired()(service: EnableBankingService) {

  @RequestMapping(path = Array("alior"))
  def test(
            @RequestParam("fromAccount") fromAccount: String,
            @RequestParam("fromUser") fromUser: String,
            @RequestParam("toAccount") toAccount: String,
            @RequestParam("toUser") toUser: String,
            @RequestParam("amount") amount: String,
            @RequestParam("title") title: String
          ) = {
    service.doTheNeedful(fromAccount, toAccount, amount, title, fromUser, toUser)
  }

  @RequestMapping(path = Array("aliorResult"))
  def result(
              @RequestParam("code") code: String,
              @RequestParam("state") state: String
            ) = {
    service.check(code, state)
  }

}
