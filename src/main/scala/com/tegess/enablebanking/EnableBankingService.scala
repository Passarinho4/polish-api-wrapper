package com.tegess.enablebanking

import java.util

import com.enablebanking.ApiClient
import com.enablebanking.api.{AuthApi, PispApi}
import com.enablebanking.model.{Access, AccountIdentification, AliorConnectorSettings, AmountType, Beneficiary, CreditTransferTransaction, HalPaymentRequestCreation, PartyIdentification, PaymentIdentification, PaymentRequestResource, PaymentTypeInformation, PostalAddress, ServiceLevelCode, UnstructuredRemittanceInformation}
import java.math.{BigDecimal => BD}

import com.tegess.api.PispJavaService

import scala.util.Try

class EnableBankingService {

  private val settings = new AliorConnectorSettings()
    .clientId("08565454-4686-4581-871f-ca3a0b7cc5af")
    .clientSecret("xU1aC1sS8qH2iA0dL6hU8tM8nY7dE8wP8pT8gI2gR5sY0rG3uD")
    .certPath(null)
    .keyPath(null)
    //.country(null)
    //.language(null)
    .paymentAuthRedirectUri("https://publo.app/alior")
    .paymentAuthState("test")
    .sandbox(true)

  private val client = new ApiClient(settings)

  private val cache = new util.HashMap[String, (String, String, String)]()

  def doTheNeedful(from: String, to: String, amount: String, title: String, fromUser: String, toUser: String) = {
    println(s"PRZELEW DO ALIORA Z ${from} DO ${to} NA KWOTE ${amount} Z TYTUŁĘM ${title}")
    val wrapper = PispJavaService.doPayment(from, to, amount, title)
    cache.put(wrapper.state, (fromUser, toUser, amount))
    wrapper.url
  }

  def check(code: String, state: String) = {
    val res = cache.get(state)
    Try {
      PispJavaService.check(code, state)
    }.recover {
      case e: Throwable => println("Exception occured" + e.getMessage)
    }
    s"${res._1},${res._2},${res._3}"
  }


  private val authApi = new AuthApi(client)

  def getAuthUrl(): String = {
    authApi.getAuth(
      "code",
      "https://publo.app/alior",
      util.Arrays.asList("aisp"),
      "random-state",
      null
    ).getUrl()
  }

}

case class EnableWrapper(api: PispApi, prr: PaymentRequestResource, creation: HalPaymentRequestCreation)

case class ResultWrapper(url: String, state: String)
