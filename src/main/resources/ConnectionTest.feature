# language: pl

Funkcja: PSD2 Testy Połączeniowe
  #Nasze certy są podpisane tym samym kluczem prywatnym QWAC_MS.ppk
  Scenariusz: Wykonanie pełnej metody PIS w celu przetesotwania przepływu informacji przez HUB PSD2
    #Zakładając że w celu uwierzytelnienia został użyty kwalifikowany certyfikat dostępu "/certs/QWAC_ALLEGRO.pem" podpisany kluczem "/certs/QWAC_ALLEGRO.ppk"
    Zakładając że w celu uwierzytelnienia został użyty kwalifikowany certyfikat dostępu "/my_certs/QWAC_MS.pem" podpisany kluczem "/my_certs/QWAC_MS.ppk"
    #Oraz że celu podpisania wiadomości został użyty kwalifikowany certyfikat pieczęci "/certs/QSEALC_ALLEGRO.pem" podpisany kluczem "/certs/QSEALC_ALLEGRO.ppk"
    Oraz że celu podpisania wiadomości został użyty kwalifikowany certyfikat pieczęci "/my_certs/QSEALC_MS.pem" podpisany kluczem "/my_certs/QWAC_MS.ppk"
    #Oraz że certyfikat pieczęci został umieszczony w internecie pod ogólnodostępnym adresem "http://www.michalpawlowski.pl:80/QSEALC_ALLEGRO.pem" w celu sprawdzenia poprawności podpisu przez HUB
    Oraz że certyfikat pieczęci został umieszczony w internecie pod ogólnodostępnym adresem "http://publo.app/QSEALC_MS.pem" w celu sprawdzenia poprawności podpisu przez HUB
    #Zakładając że API ASPSP znajduje się pod adresem "https://api-obh.kir.pl"
    Zakładając że API ASPSP znajduje się pod adresem "https://api-obh.kir.pl"
    #Oraz do banku certyfikatów zostało dodane CA: "/certs/HUB_PSD2_TEST_SUB_CA1.chain.cer"
    Oraz do banku certyfikatów zostało dodane CA: "/my_certs/HUB_PSD2_TEST_SUB_CA1.chain.cer"
    Oraz że timeouty klienta został ustawione w następujący sposób:
        | connectTimeout | 6000 |
        | readTimeout    | 6000 |
        | writeTimeout   | 6000 |
    Oraz że do zapytania zostały dodane następujące nagłówki:
        | X-ASPSP-NAME   | Mock TPP CA1 sp. z o.o. |
    Oraz że w poniższych polch zapytań zostały ustalone następujące wartości:
        | redirectUri    | https://bank-obh.kir.pl/#/login/ |
        | userAgent      | "Swagger-Codegen/1.0.0/java"     |
    Zakładając że tworzymy scope typu "PIS" ważny przez "180" sekund z następującymi uprawnieniami:
        """
        {"privilegeList":[
          {"pis:domestic":{
            "scopeUsageLimit":"single",
            "recipient":{
              "accountNumber":"06551905220000000000023717",
              "nameAddress":{"value":["Bartosz Banasik","Wscieklych Wezy 5/10","Arlamow, 00-007","Polska"]}
            },
            "sender":{
              "accountNumber":"35551905220000000000019315",
              "nameAddress":{"value":["Grzegorz Brzeczyszczykiewicz","Pomaranczowych domow 0/1","Mediolanek, 01-234","Polska"]}
            },
            "transferData":{"description":"DOMESTIC transfer","amount":"100.00","executionDate":"%LocalDate%","currency":"PLN"},
            "deliveryMode":"StandardD1",
            "system":"Elixir",
            "executionMode":"Immediate"
          }}
        ]}
        """
    Oraz wykonujemy metode caf:confirmationOfFunds w celu zweryfikowania czy na koncie są odpowiednie środki
    Wtedy zostaje wywołana metoda as:authorize w celu autoryzacji i uzyskania zgody na dalsze requesty
    I w odpowiedzi został przesłany link do otrzymania tokena
    Wtedy wycinamy z linka requestId wg wzoru "login/(.*)"
    I wykonujemy procedurę uzyskania tokenu manulanie
    Wtedy zostaje wywołana metoda as:token w celu uzyskania access_tokena
    Wtedy wykonujemy metodę pis:domestic z dzisiejszą datą z parametrami okreśolnymi w zgodzie

    Wtedy tworzymy scopedetails dla zapytania pis:getPayment z danymi z pis:domestic
    Wtedy zostaje wywołana metoda as:authorize w celu autoryzacji i uzyskania zgody na dalsze requesty
    I w odpowiedzi został przesłany link do otrzymania tokena
    Wtedy wycinamy z linka requestId wg wzoru "login/(.*)"
    I wykonujemy procedurę uzyskania tokenu manulanie
    Wtedy zostaje wywołana metoda as:token w celu uzyskania access_tokena
    Oraz wykonujemy metodę pis:getPayment w celu zweryfikowania czy płatność przeszła prawidłowo
