package pl.kir.psd2.tpp.conn.internal;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.Base64;

public class GsonConfig {

    public static Gson get() {
        return new GsonBuilder()
                .registerTypeAdapter(OffsetDateTime.class, new TypeAdapter<OffsetDateTime>() {
                    private final DateTimeFormatter writeFormatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME;
                    private final DateTimeFormatter readFormatter = new DateTimeFormatterBuilder()
                            .append(DateTimeFormatter.ISO_LOCAL_DATE_TIME)
                            .optionalStart().appendOffset("+HH:MM", "+00:00").optionalEnd()
                            .optionalStart().appendOffset("+HHMM", "+0000").optionalEnd()
                            .optionalStart().appendOffset("+HH", "Z").optionalEnd()
                            .toFormatter();

                    @Override
                    public void write(JsonWriter out, OffsetDateTime date) throws IOException {
                        out.value(date == null ? null : writeFormatter.format(date));
                    }

                    @Override
                    public OffsetDateTime read(JsonReader reader) throws IOException {
                        if (reader.peek() != JsonToken.NULL) {
                            return OffsetDateTime.parse(reader.nextString(), readFormatter);
                        } else {
                            reader.nextNull();
                            return null;
                        }
                    }
                }).registerTypeAdapter(LocalDate.class, new TypeAdapter<LocalDate>() {
                    private final DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_DATE;

                    @Override
                    public void write(JsonWriter out, LocalDate date) throws IOException {
                        out.value(date == null ? null : formatter.format(date));
                    }

                    @Override
                    public LocalDate read(JsonReader reader) throws IOException {
                        if (reader.peek() != JsonToken.NULL) {
                            return LocalDate.parse(reader.nextString(), formatter);
                        } else {
                            reader.nextNull();
                            return null;
                        }
                    }
                }).registerTypeAdapter(byte[].class, new TypeAdapter<byte[]>() {

                    @Override
                    public void write(JsonWriter out, byte[] value) throws IOException {
                        if (value == null) {
                            out.nullValue();
                        } else {
                            out.value(Base64.getEncoder().encodeToString(value));
                        }
                    }


                    @Override
                    public byte[] read(JsonReader in) throws IOException {
                        if (in.peek() != JsonToken.NULL) {
                            return Base64.getDecoder().decode(in.nextString());
                        } else {
                            in.nextNull();
                            return null;
                        }
                    }
                })
                //.serializeNulls()
                .create();
    }

}
