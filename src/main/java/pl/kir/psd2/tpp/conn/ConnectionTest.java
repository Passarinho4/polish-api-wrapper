package pl.kir.psd2.tpp.conn;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(plugin = {"pretty"}, features = "src/main/resources/ConnectionTest.feature")
public class ConnectionTest {}
