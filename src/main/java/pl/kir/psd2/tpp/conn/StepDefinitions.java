package pl.kir.psd2.tpp.conn;

import com.google.gson.Gson;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.squareup.okhttp.ResponseBody;
import cucumber.api.java.pl.*;
import io.cucumber.datatable.DataTable;
import okio.Buffer;
import okio.BufferedSource;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.RandomStringUtils;
import pl.kir.psd2.tpp.conn.api.ScopeDetailsHelper;
import pl.kir.psd2.tpp.conn.crypto.BodySigner;
import pl.kir.psd2.tpp.conn.crypto.KeyPair;
import pl.kir.psd2.tpp.conn.crypto.SignatureVerifier;
import pl.kir.psd2.tpp.conn.internal.*;
import pl.kir.psd2.tpp.polishapi.ApiClient;
import pl.kir.psd2.tpp.polishapi.ApiResponse;
import pl.kir.psd2.tpp.polishapi.api.AsApi;
import pl.kir.psd2.tpp.polishapi.api.CafApi;
import pl.kir.psd2.tpp.polishapi.api.PisApi;
import pl.kir.psd2.tpp.polishapi.model.*;
import pl.kir.psd2.tpp.polishapi.model.AddPaymentResponse;
import pl.kir.psd2.tpp.polishapi.model.AuthorizeRequest;
import pl.kir.psd2.tpp.polishapi.model.AuthorizeResponse;
import pl.kir.psd2.tpp.polishapi.model.ConfirmationOfFundsRequest;
import pl.kir.psd2.tpp.polishapi.model.ConfirmationOfFundsResponse;
import pl.kir.psd2.tpp.polishapi.model.GetPaymentResponse;
import pl.kir.psd2.tpp.polishapi.model.PaymentDomesticRequest;
import pl.kir.psd2.tpp.polishapi.model.PaymentRequest;
import pl.kir.psd2.tpp.polishapi.model.PrivilegePayment;
import pl.kir.psd2.tpp.polishapi.model.ScopeDetailsInput.*;
import pl.kir.psd2.tpp.polishapi.model.ScopeDetailsInputPrivilegeList;
import pl.kir.psd2.tpp.polishapi.model.TokenRequest;
import pl.kir.psd2.tpp.polishapi.model.TokenResponse;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.swing.*;
import java.io.IOException;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.time.OffsetDateTime;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StepDefinitions {
    private Gson gson = GsonConfig.get();
    private KeyPair qwacKeyPair;
    private ScopeDetailsHelper sdh = new ScopeDetailsHelper();

    @Zakładając("^że w celu uwierzytelnienia został użyty kwalifikowany certyfikat dostępu \"([^\"]*)\" podpisany kluczem \"([^\"]*)\"$")
    public void certificateSetupQWAC(String certificate, String privateKey) throws Exception {
        qwacKeyPair = KeyPair.create(
                IOUtils.toString(getClass().getResourceAsStream(certificate), StandardCharsets.UTF_8),
                IOUtils.toString(getClass().getResourceAsStream(privateKey), StandardCharsets.UTF_8));
    }

    private KeyPair qSealCKeyPair;

    @Oraz("^że celu podpisania wiadomości został użyty kwalifikowany certyfikat pieczęci \"([^\"]*)\" podpisany kluczem \"([^\"]*)\"$")
    public void certificateSetupQsealC(String certificate, String privateKey) throws Exception {
        qSealCKeyPair = KeyPair.create(
                IOUtils.toString(getClass().getResourceAsStream(certificate), StandardCharsets.UTF_8),
                IOUtils.toString(getClass().getResourceAsStream(privateKey), StandardCharsets.UTF_8));
    }

    private BodySigner bodySigner;

    @Oraz("^że certyfikat pieczęci został umieszczony w internecie pod ogólnodostępnym adresem \"([^\"]*)\" w celu sprawdzenia poprawności podpisu przez HUB$")
    public void signerInitialization(String publicKeyUrl) throws URISyntaxException {
        bodySigner = new BodySigner(qSealCKeyPair, new URI(publicKeyUrl), gson);
    }

    private ApiClient apiClient;

    @Zakładając("^że API ASPSP znajduje się pod adresem \"([^\"]*)\"$")
    public void clientConfiguration(String serverUrl) {
        apiClient = new ApiClient();
        apiClient.setBasePath(serverUrl);
        apiClient.setDebugging(true);
        apiClient.getJSON().setGson(gson);
        apiClient.getHttpClient().interceptors().add(new RawResponseInterceptor());
    }

    @Oraz("^do banku certyfikatów zostało dodane CA: \"([^\"]*)\"$")
    public void addCA(String CACertPath) throws Exception {
        if (apiClient.getBasePath().startsWith("https")) {
            apiClient.setSslCaCert(null);
            apiClient.setKeyManagers(qwacKeyPair.createKeyManager());
            apiClient.setSslCaCert(getClass().getResourceAsStream(CACertPath));
        }
    }

    @Oraz("^że timeouty klienta został ustawione w następujący sposób:$")
    public void setTimeouts(DataTable dt) {
        Map<String, Integer> timeouts = dt.asMap(String.class, Integer.class);
        apiClient.setConnectTimeout(timeouts.getOrDefault("connectTimeout", 6000));
        apiClient.setReadTimeout(timeouts.getOrDefault("readTimeout", 6000));
        apiClient.setWriteTimeout(timeouts.getOrDefault("writeTimeout", 6000));
    }

    @Oraz("^że do zapytania zostały dodane następujące nagłówki:$")
    public void addHeaders(DataTable dt) {
        Map<String, String> headers = dt.asMap(String.class, String.class);
        for (Map.Entry<String, String> header : headers.entrySet()) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> chain.proceed(chain.request().newBuilder()
                    .addHeader(header.getKey(), header.getValue()).build()));
        }
    }

    @Oraz("^że w poniższych polch zapytań zostały ustalone następujące wartości:$")
    public void environmentSettings(DataTable dt) {
        Map<String, String> env = dt.asMap(String.class, String.class);

        sdh.setTppId((env.get("tppId") != null) ? env.get("tppId") :
                    (qSealCKeyPair != null ?
                            qSealCKeyPair.getTppIdFromX509() :
                            qwacKeyPair.getTppIdFromX509()));

        sdh.setRedirectUrl(env.get("redirectUri"));
    }

    private PrivilegeDomesticTransfer privilegeDomesticTransfer;

    @Oraz("^że tworzymy scope typu \"([^\"]*)\" ważny przez \"(\\d+)\" sekund z następującymi uprawnieniami:$")
    public void scopeGroupDefine(String groupType, int expireSeconds, String scopeDetailsJson) throws Exception {
        sdh.instantiateConsentFromJSON(groupType, expireSeconds, scopeDetailsJson);

        for (ScopeDetailsInputPrivilegeList sdi : sdh.getPrivilegeList()) {
            privilegeDomesticTransfer = sdi.getPisdomestic();
            if (privilegeDomesticTransfer != null) break;
        }

        if (privilegeDomesticTransfer == null) throw new Exception("No premission to perform PIS Domestic request");
    }

    @Oraz("^wykonujemy metode caf:confirmationOfFunds w celu zweryfikowania czy na koncie są odpowiednie środki$")
    public void confirmationOfFunds() throws Exception {
        ConfirmationOfFundsRequest confirmationOfFundsRequest = new ConfirmationOfFundsRequest();
        confirmationOfFundsRequest.setRequestHeader(sdh.getRequestHeaderWithoutToken());
        confirmationOfFundsRequest.accountNumber(privilegeDomesticTransfer.getSender().getAccountNumber());
        confirmationOfFundsRequest.amount(privilegeDomesticTransfer.getTransferData().getAmount());
        confirmationOfFundsRequest.currency(privilegeDomesticTransfer.getTransferData().getCurrency());

        CafApi api = new CafApi(apiClient);
        ApiResponse<ConfirmationOfFundsResponse> confirmationOfFundsResponseApiResponse = api.getConfirmationOfFundsWithHttpInfo(
                "deflate",
                "en-gb",
                StandardCharsets.UTF_8.displayName(),
                bodySigner.sign(confirmationOfFundsRequest),
                confirmationOfFundsRequest.getRequestHeader().getRequestId().toString(),
                confirmationOfFundsRequest
        );

        if (confirmationOfFundsResponseApiResponse.getStatusCode() != 200)
            throw new Exception("Metoda caf:confirmationOfFunds odpowiedziała kodem: " + confirmationOfFundsResponseApiResponse.getStatusCode());

        checkJWSSignature(confirmationOfFundsResponseApiResponse);
    }

    private ApiResponse<AuthorizeResponse> authorizeResponseApiResponse;

    @Wtedy("^zostaje wywołana metoda as:authorize w celu autoryzacji i uzyskania zgody na dalsze requesty$")
    public void executeAsAuthorize() throws Exception {
        AuthorizeRequest authorizeRequest = new AuthorizeRequest();
        authorizeRequest.setRequestHeader(sdh.getRequestHeaderWithoutTokenAS());
        authorizeRequest.setResponseType("code");
        authorizeRequest.clientId(sdh.getTppId());
        authorizeRequest.setRedirectUri(sdh.getRedirectUrl());
        authorizeRequest.setScope(sdh.getScopeType());
        authorizeRequest.setState(UUID.randomUUID().toString());
        authorizeRequest.setScopeDetails(sdh.getScopeDetailsInput());

        AsApi api = new AsApi(apiClient);
        authorizeResponseApiResponse = api.authorizeWithHttpInfo(
                "deflate",
                "en-gb",
                StandardCharsets.UTF_8.displayName(),
                bodySigner.sign(authorizeRequest),
                authorizeRequest.getRequestHeader().getRequestId().toString(),
                authorizeRequest
        );

        if (authorizeResponseApiResponse.getStatusCode() != 200)
            throw new Exception("Metoda as:authorize odpowiedziała kodem: " + authorizeResponseApiResponse.getStatusCode());

        checkJWSSignature(authorizeResponseApiResponse);
    }

    private String recievedRedirectUri;

    @I("^w odpowiedzi został przesłany link do otrzymania tokena$")
    public void extractAuthcode() throws Exception {
        recievedRedirectUri = authorizeResponseApiResponse.getData().getAspspRedirectUri();
        if (recievedRedirectUri == null || recievedRedirectUri.equals(""))
            throw new Exception("no consent url send in Authorize response");
    }

    private String redirectDomain;
    private String authRequestId;

    @Wtedy("^wycinamy z linka requestId wg wzoru \"([^\"]*)\"$")
    public void getConsent(String tokenPattern) throws Exception {
        redirectDomain = sdh.getRedirectUrl();
        if (redirectDomain.length() > 9)
            redirectDomain = recievedRedirectUri.substring(0, recievedRedirectUri.indexOf("/", 9));

        Pattern r = Pattern.compile(tokenPattern);
        Matcher m = r.matcher(recievedRedirectUri);
        if (!m.find()) throw new RuntimeException("Fetch Login Token with regex: " + tokenPattern + " - no match");
        authRequestId = m.group(1);

        if (authRequestId.equals("")) throw new Exception("Brak tokenu zwróconego przez metodę Authorize");
    }

    private String token;

    @I("wykonujemy procedurę uzyskania tokenu manulanie")
    public void manualExecuteConsentLinkToObtainToken() throws Exception {
        String natTranslatedLink = sdh.getRedirectUrl() + authRequestId;//recievedRedirectUri.substring(recievedRedirectUri.indexOf("?"));

        System.out.println("///////////////////////////////////////////////////////////////////////////////////////\n//");
        System.out.println("//\t\tKliknij w poniższy link i po wyrażeniu zgód wklej link zwrotny z authorize code:");
        System.out.println("//\t\t" + natTranslatedLink);
        System.out.println("//\t\tcurl -sSL -D - " + natTranslatedLink);
        System.out.println("//\n///////////////////////////////////////////////////////////////////////////////////////");

        JFrame frame = new JFrame("Token");
        frame.setUndecorated( true );
        frame.setVisible( true );
        frame.setLocationRelativeTo( null );
        token = JOptionPane.showInputDialog(frame, "Link zwrotny z authorize code:");
        frame.dispose();

        if (token.equals("")) throw new Exception("Brak tokenu zwróconego przez metodę Authorize");

        int tokenStart = token.indexOf("code=")+5;
        token = token.substring(tokenStart, token.indexOf("&", tokenStart));
    }

    @Wtedy("^zostaje wywołana metoda as:token w celu uzyskania access_tokena")
    public void executeAsToken() throws Exception {
        TokenRequest tokenRequest = new TokenRequest();
        tokenRequest.setRequestHeader(sdh.getRequestHeaderWithoutTokenAS());
        tokenRequest.setGrantType("authorization_code");
        tokenRequest.setCode(token);
        tokenRequest.setRedirectUri(sdh.getRedirectUrl());
        tokenRequest.setClientId(sdh.getTppId());
        tokenRequest.setScope(sdh.getScopeType());
        tokenRequest.setScopeDetails(sdh.getScopeDetailsInput());
        tokenRequest.setIsUserSession(true);
        tokenRequest.setUserIp(InetAddress.getLocalHost().getHostAddress());

        AsApi api = new AsApi(apiClient);
        ApiResponse<TokenResponse> tokenResponseApiResponse = api.tokenWithHttpInfo(
                "deflate",
                "en-gb",
                StandardCharsets.UTF_8.displayName(),
                bodySigner.sign(tokenRequest),
                tokenRequest.getRequestHeader().getRequestId().toString(),
                tokenRequest
        );

        if (tokenResponseApiResponse.getStatusCode() != 200)
            throw new Exception("Metoda as:token odpowiedziała kodem: " + tokenResponseApiResponse.getStatusCode());

        sdh.setAccessToken(tokenResponseApiResponse.getData().getAccessToken());

        checkJWSSignature(tokenResponseApiResponse);
    }

    private String paymentId;
    private String tppTransactionId;

    @Wtedy("^wykonujemy metodę pis:domestic z dzisiejszą datą z parametrami okreśolnymi w zgodzie$")
    public void createPISRequest() throws Exception {
        PaymentDomesticRequest paymentDomesticRequest = new PaymentDomesticRequest();
        paymentDomesticRequest.setRequestHeader(sdh.getRequestHeaderCallback());
        paymentDomesticRequest.setRecipient(privilegeDomesticTransfer.getRecipient());
        paymentDomesticRequest.setSender(privilegeDomesticTransfer.getSender());
        paymentDomesticRequest.setTransferData(privilegeDomesticTransfer.getTransferData());
        tppTransactionId = RandomStringUtils.random(12, true, true);
        paymentDomesticRequest.setTppTransactionId(tppTransactionId);
        paymentDomesticRequest.setDeliveryMode(PaymentDomesticRequest.DeliveryModeEnum.fromValue(privilegeDomesticTransfer.getDeliveryMode().getValue()));
        paymentDomesticRequest.setSystem(PaymentDomesticRequest.SystemEnum.fromValue(privilegeDomesticTransfer.getSystem().getValue()));
        paymentDomesticRequest.setHold(false);
        paymentDomesticRequest.setExecutionMode(PaymentDomesticRequest.ExecutionModeEnum.fromValue(privilegeDomesticTransfer.getExecutionMode().getValue()));

        PisApi api = new PisApi(apiClient);
        ApiResponse<AddPaymentResponse> addPaymentResponse = api.domesticWithHttpInfo(
                "Bearer " + sdh.getAccessToken(),
                "deflate",
                "en-gb",
                StandardCharsets.UTF_8.displayName(),
                bodySigner.sign(paymentDomesticRequest),
                paymentDomesticRequest.getRequestHeader().getRequestId().toString(),
                paymentDomesticRequest
        );

        if (addPaymentResponse.getStatusCode() != 200)
            throw new Exception("Metoda pis:domestic odpowiedziała kodem: " + addPaymentResponse.getStatusCode());

        paymentId = addPaymentResponse.getData().getPaymentId();

        checkJWSSignature(addPaymentResponse);
    }

    @Wtedy("^tworzymy scopedetails dla zapytania pis:getPayment z danymi z pis:domestic$")
    public void createScopeDetailsForGetPayment() {
        PrivilegePayment privilegePayment = new PrivilegePayment();
        privilegePayment.setScopeUsageLimit(PrivilegePayment.ScopeUsageLimitEnum.SINGLE);
        privilegePayment.setPaymentId(paymentId);
        privilegePayment.setTppTransactionId(tppTransactionId);

        ScopeDetailsInputPrivilegeList scopeDetailsInputPrivilegeList = new ScopeDetailsInputPrivilegeList();
        scopeDetailsInputPrivilegeList.setPisgetPayment(privilegePayment);

        sdh.setScopeType(ScopeGroupTypeEnum.PIS);
        sdh.newConsentId();
        sdh.resetScopeDetailsInputPrivilegeList();
        sdh.addScopeDetailsInputPrivilegeList(scopeDetailsInputPrivilegeList);
        sdh.setScopeTimeLimit(OffsetDateTime.now().plusSeconds(180));
    }

    @Oraz("^wykonujemy metodę pis:getPayment w celu zweryfikowania czy płatność przeszła prawidłowo$")
    public void getPayment() throws Exception {
        PaymentRequest paymentRequest = new PaymentRequest();
        paymentRequest.setRequestHeader(sdh.getRequestHeader());
        paymentRequest.setPaymentId(paymentId);
        paymentRequest.setTppTransactionId(tppTransactionId);

        PisApi api = new PisApi(apiClient);
        ApiResponse<GetPaymentResponse> getPaymentResponseApiResponse = api.getPaymentWithHttpInfo(
                "Bearer " + sdh.getAccessToken(),
                "deflate",
                "en-gb",
                StandardCharsets.UTF_8.displayName(),
                bodySigner.sign(paymentRequest),
                paymentRequest.getRequestHeader().getRequestId().toString(),
                paymentRequest
        );

        if (getPaymentResponseApiResponse.getStatusCode() != 200)
            throw new Exception("Metoda pis:getPayment odpowiedziała kodem: " + getPaymentResponseApiResponse.getStatusCode());

        checkJWSSignature(getPaymentResponseApiResponse);
    }

    SignatureVerifier signatureVerifier = new SignatureVerifier();

    private void checkJWSSignature(ApiResponse response) throws Exception {
        @SuppressWarnings("unchecked")
        List<String> jwsList = (List<String>) response.getHeaders().get("X-JWS-SIGNATURE");
        if (jwsList == null || jwsList.isEmpty())
            throw new Exception("No JWS Signature");

        String rawResponse = "";
        for (Interceptor interceptor : apiClient.getHttpClient().interceptors())
            if (interceptor instanceof RawResponseInterceptor)
                rawResponse = ((RawResponseInterceptor) interceptor).lastResponse;

        System.out.println("Sprawdzam poprawność odpowiedzi - sygnartura JWS:");
        System.out.println(jwsList.get(0));
        System.out.println("Niemodyfikowana odpowiedź:");
        System.out.println(rawResponse);
        System.out.println("Wynik: " + signatureVerifier.verifySignature(jwsList.get(0), rawResponse));

        if (!signatureVerifier.verifySignature(jwsList.get(0), rawResponse))
            throw new Exception("Verified JWS signature is incorrect");
    }


}
