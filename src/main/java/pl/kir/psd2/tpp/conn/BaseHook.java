package pl.kir.psd2.tpp.conn;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.logging.HttpLoggingInterceptor;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import java.security.Security;

public class BaseHook {

    @Before
    public void before() {
        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.interceptors().add(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY));

        Security.addProvider(new BouncyCastleProvider());
    }

}
