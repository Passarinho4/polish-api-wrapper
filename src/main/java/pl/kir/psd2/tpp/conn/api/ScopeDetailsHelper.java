package pl.kir.psd2.tpp.conn.api;

import com.fasterxml.uuid.Generators;
import com.google.gson.Gson;
import pl.kir.psd2.tpp.conn.internal.GsonConfig;
import pl.kir.psd2.tpp.polishapi.model.*;
import pl.kir.psd2.tpp.polishapi.model.RequestHeader;
import pl.kir.psd2.tpp.polishapi.model.RequestHeaderCallback;
import pl.kir.psd2.tpp.polishapi.model.RequestHeaderWithoutToken;
import pl.kir.psd2.tpp.polishapi.model.RequestHeaderWithoutTokenAS;
import pl.kir.psd2.tpp.polishapi.model.ScopeDetailsInput;
import pl.kir.psd2.tpp.polishapi.model.ScopeDetailsInputPrivilegeList;

import static pl.kir.psd2.tpp.polishapi.model.ScopeDetailsInput.*;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.UUID;

public class ScopeDetailsHelper {
    private Gson gson = GsonConfig.get();
    private String tppId = "TPP_ID_1";
    private String redirectUrl = "http://example.com";
    private String userAgent = "Swagger-Codegen/1.0.0/java";
    private String ip;
    private String accessToken;
    private ScopeDetailsInput scopeDetailsInput;
    private String scopeType = "AIS";
    private ScopeGroupTypeEnum scopeTypeEnum = ScopeGroupTypeEnum.AIS;

    public ScopeDetailsHelper() {
        try {
            this.ip = InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {e.printStackTrace();}
        scopeDetailsInput = new ScopeDetailsInput();
        scopeDetailsInput.setThrottlingPolicy(ScopeDetailsInput.ThrottlingPolicyEnum.PSD2REGULATORY);
        newConsentId();
    }

    public ScopeDetailsHelper(String tppId, String redirectUrl) {
        this();
        this.setTppId(tppId);
        this.setRedirectUrl(redirectUrl);
    }

    public void instantiateConsentFromJSON(String scopeType, int expireSeconds, String scopeDetailsJson) {
        scopeDetailsJson = scopeDetailsJson.replaceAll("%LocalDate%", LocalDate.now().toString());

        this.scopeType = scopeType.toUpperCase();
        this.scopeTypeEnum = ScopeDetailsInput.ScopeGroupTypeEnum.valueOf(this.scopeType);

        scopeDetailsInput = gson.fromJson(scopeDetailsJson, ScopeDetailsInput.class);
        scopeDetailsInput.setScopeGroupType(this.scopeTypeEnum);
        scopeDetailsInput.setThrottlingPolicy(ScopeDetailsInput.ThrottlingPolicyEnum.PSD2REGULATORY);
        setScopeTimeLimit(OffsetDateTime.now().plusSeconds(expireSeconds));
        newConsentId();
    }

    public ScopeDetailsInput getScopeDetailsInput() {
        return scopeDetailsInput;
    }

    public void newConsentId() {
        scopeDetailsInput.setConsentId(UUID.randomUUID().toString());
    }

    public void setScopeTimeLimit(OffsetDateTime offsetDateTime) {
        scopeDetailsInput.setScopeTimeLimit(offsetDateTime);
    }

    public String getScopeType() {
        return scopeType.toLowerCase();
    }

    public void setScopeType(String scopeType) {
        this.scopeType = scopeType;
        this.scopeTypeEnum = ScopeDetailsInput.ScopeGroupTypeEnum.valueOf(this.scopeType);
        scopeDetailsInput.setScopeGroupType(this.scopeTypeEnum);
    }

    public void setScopeType(ScopeGroupTypeEnum scopeType) {
        this.scopeType = scopeType.getValue();
        this.scopeTypeEnum = scopeType;
        scopeDetailsInput.setScopeGroupType(scopeType);
    }

    public RequestHeaderWithoutToken getRequestHeaderWithoutToken() {
        RequestHeaderWithoutToken requestHeaderWithoutToken = new RequestHeaderWithoutToken();
        requestHeaderWithoutToken.setRequestId(Generators.timeBasedGenerator().generate());
        requestHeaderWithoutToken.setUserAgent(userAgent);
        requestHeaderWithoutToken.setIpAddress(ip);
        requestHeaderWithoutToken.setSendDate(OffsetDateTime.now());
        requestHeaderWithoutToken.setTppId(tppId);
        return requestHeaderWithoutToken;
    }

    public RequestHeaderWithoutTokenAS getRequestHeaderWithoutTokenAS() {
        RequestHeaderWithoutTokenAS requestHeaderWithoutTokenAS = new RequestHeaderWithoutTokenAS();
        requestHeaderWithoutTokenAS.setRequestId(Generators.timeBasedGenerator().generate());
        requestHeaderWithoutTokenAS.setUserAgent(userAgent);
        requestHeaderWithoutTokenAS.setIpAddress(ip);
        requestHeaderWithoutTokenAS.setSendDate(OffsetDateTime.now());
        requestHeaderWithoutTokenAS.setTppId(tppId);
        return requestHeaderWithoutTokenAS;
    }

    public RequestHeaderCallback getRequestHeaderCallback() {
        RequestHeaderCallback requestHeaderCallback = new RequestHeaderCallback();
        requestHeaderCallback.setRequestId(Generators.timeBasedGenerator().generate());
        requestHeaderCallback.setUserAgent(userAgent);
        requestHeaderCallback.setIpAddress(ip);
        requestHeaderCallback.setSendDate(OffsetDateTime.now());
        requestHeaderCallback.setTppId(tppId);
        if (accessToken != null) requestHeaderCallback.setToken(accessToken);
        requestHeaderCallback.setApiKey(UUID.randomUUID().toString());
        requestHeaderCallback.setCallbackURL("http://foo.bar/api");
        return requestHeaderCallback;
    }

    public RequestHeader getRequestHeader() {
        RequestHeader requestHeader = new RequestHeader();
        requestHeader.setRequestId(Generators.timeBasedGenerator().generate());
        requestHeader.setIpAddress(ip);
        requestHeader.setSendDate(OffsetDateTime.now());
        requestHeader.setTppId(tppId);
        requestHeader.setUserAgent(userAgent);
        if (accessToken != null) requestHeader.setToken(accessToken);
        return requestHeader;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public void resetScopeDetailsInputPrivilegeList() {
        scopeDetailsInput.getPrivilegeList().removeAll(scopeDetailsInput.getPrivilegeList());
    }

    public void addScopeDetailsInputPrivilegeList(ScopeDetailsInputPrivilegeList scopeDetailsInputPrivilegeList) {
        scopeDetailsInput.addPrivilegeListItem(scopeDetailsInputPrivilegeList);
    }

    public List<ScopeDetailsInputPrivilegeList> getPrivilegeList() {
        return scopeDetailsInput.getPrivilegeList();
    }

    public String getTppId() {
        return tppId;
    }

    public void setTppId(String tppId) {
        this.tppId = tppId;
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }
}
