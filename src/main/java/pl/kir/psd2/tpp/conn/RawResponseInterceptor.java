package pl.kir.psd2.tpp.conn;

import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.squareup.okhttp.ResponseBody;
import okio.Buffer;
import okio.BufferedSource;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class RawResponseInterceptor implements Interceptor {
    public String lastResponse;

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        Response response = chain.proceed(request);
        ResponseBody responseBody = response.body();
        BufferedSource source = responseBody.source();
        source.request(Long.MAX_VALUE); // Buffer the entire body.
        Buffer buffer = source.buffer();
        lastResponse = buffer.clone().readString(StandardCharsets.UTF_8);
        //System.out.println("ret ==> " + buffer.clone().readString(StandardCharsets.UTF_8));
        return response;
    }
}
