package pl.kir.psd2.tpp.conn.crypto;

import org.bouncycastle.openssl.PEMKeyPair;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;

import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import java.io.*;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Arrays;

public class KeyPair {
    private static final char[] KEY_MANAGER_PASSWORD = "".toCharArray();
    private static final char[] PRIVATE_KEY_PASSWORD = "".toCharArray();
    private byte[] certificateByte;
    private Certificate certificate;
    private PrivateKey privateKey;

    private KeyPair(byte[] certificate, byte[] privateKey) {
        this.certificateByte = certificate;
        this.certificate = createPublicKey(certificate);
        this.privateKey = createPrivateKey(privateKey);
    }

    public static KeyPair createFromStream(File certificate, File privateKey) throws IOException {
        return createFromFile(Paths.get(certificate.getAbsolutePath()), Paths.get(privateKey.getAbsolutePath()));
    }

    public static KeyPair createFromFile(File certificate, File privateKey) throws IOException {
        return createFromFile(Paths.get(certificate.getAbsolutePath()), Paths.get(privateKey.getAbsolutePath()));
    }

    public static KeyPair createFromFile(Path certificate, Path privateKey) throws IOException {
        return create(Files.readAllBytes(certificate), Files.readAllBytes(privateKey));
    }

    public static KeyPair create(String certificate, String privateKey) {
        return create(certificate.getBytes(), privateKey.getBytes());
    }

    public static KeyPair create(byte[] certificate, byte[] privateKey) {
        return new KeyPair(certificate, privateKey);
    }

    public Certificate getCertificate()  {
        if (certificate == null) throw new NullPointerException("Certificate is null");
        return certificate;
    }

    public X509Certificate getX509Certificate() {
        if (certificate == null) throw new NullPointerException("Certificate is null");
        return (X509Certificate) certificate;
    }

    public PrivateKey getPrivateKey() {
        if (privateKey == null) throw new NullPointerException("PrivateKey is null");
        return privateKey;
    }

    public String getUrlEncodedCertificate() throws UnsupportedEncodingException {
        return URLEncoder.encode(new String(certificateByte, StandardCharsets.UTF_8), "UTF-8");
    }

    public String getTppIdFromX509() {
        // Get certificate DN:
        String subject = getX509Certificate().getSubjectDN().getName();

        // Get correct entry from name: OID.2.5.4.97=PSDPL-KNF-123456
        return Arrays.stream(subject.split(","))
                .filter(s -> s.contains("OID"))
                .map(s -> s.split("=")[1])
                .findAny()
                .orElseThrow(() -> new RuntimeException("TPP ID not found in Subject DN -> OID"));
    }

    public KeyManager[] createKeyManager() throws CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException, UnrecoverableKeyException {
        KeyStore ks = KeyStore.getInstance("JKS");
        ks.load(null);
        ks.setCertificateEntry("client-certificate", certificate);
        ks.setKeyEntry("client-key", privateKey, PRIVATE_KEY_PASSWORD, new Certificate[]{certificate});

        KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
        kmf.init(ks, KEY_MANAGER_PASSWORD);

        return kmf.getKeyManagers();
    }

    private Certificate createPublicKey(byte[] certificate) {
        try {
            return CertificateFactory.getInstance("X.509").generateCertificate(new ByteArrayInputStream(certificate));
        } catch (CertificateException e) {
            e.printStackTrace();
            return null;
        }
    }

    private PrivateKey createPrivateKey(byte[] privateKey) {
        try {
            Object pemKey = new PEMParser(new StringReader(new String(privateKey, StandardCharsets.UTF_8))).readObject();
            JcaPEMKeyConverter createConverter = new JcaPEMKeyConverter().setProvider("BC");
            java.security.KeyPair keyPair = createConverter.getKeyPair((PEMKeyPair) pemKey);
            return keyPair.getPrivate();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

}