package pl.kir.psd2.tpp.conn.crypto;

import com.google.gson.Gson;
import com.nimbusds.jose.*;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jose.util.Base64URL;
import org.bouncycastle.util.encoders.Hex;

import java.net.URI;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.Optional;

public class BodySigner {
    private Gson gson;
    private KeyPair qsealcKeyPair;
    private URI qsealcWebLocation;

    public BodySigner(KeyPair qsealcKeyPair, URI qsealcWebLocation, Gson gson) {
        this.gson = gson;
        this.qsealcKeyPair = qsealcKeyPair;
        this.qsealcWebLocation = qsealcWebLocation;
    }

    public String sign(Object body) throws CertificateEncodingException, NoSuchAlgorithmException {
        return serializedJwsOf(body);
    }

    private String serializedJwsOf(Object body) throws CertificateEncodingException, NoSuchAlgorithmException {
        String jsonOf = this.gson.toJson(body);
        String it = createSignedJwsFor(jsonOf).serialize();
        return removePayload(it);
    }

    private String removePayload(String jwsString) {
        return jwsString.replaceAll("\\.(.*?)\\.", "..");
    }

    private JWSObject createSignedJwsFor(String body) throws CertificateEncodingException, NoSuchAlgorithmException {
        JWSObject jwsObject = new JWSObject(buildJwsHeader(), new Payload(body));
        try {
            jwsObject.sign(new RSASSASigner(qsealcKeyPair.getPrivateKey()));
            return jwsObject;
        } catch (JOSEException e) {
            throw new RuntimeException(e);
        }
    }

    private JWSHeader buildJwsHeader() throws CertificateEncodingException, NoSuchAlgorithmException {
        return new JWSHeader
                .Builder(JWSAlgorithm.RS256)
                .type(JOSEObjectType.JWT)
                .x509CertURL(qsealcWebLocation)
                .keyID(CertificateKidExtractor.getKid(qsealcKeyPair.getX509Certificate()))
                .x509CertSHA256Thumbprint(CertificateHashExtractor.getHash(qsealcKeyPair.getX509Certificate()))
                .build();
    }

}

class CertificateKidExtractor {
    private static final String KEY_ID_EXTENSION_OID = "2.5.29.14";
    private static final int OCTET_STRING_METADATA_LENGTH = 8;

    static String getKid(X509Certificate x509Certificate) {
        return Optional.ofNullable(x509Certificate
                .getExtensionValue(KEY_ID_EXTENSION_OID))
                .map(Hex::toHexString)
                .map(e -> e.substring(OCTET_STRING_METADATA_LENGTH))
                .orElseThrow(() -> new IllegalArgumentException("Key id not present in certificate"));
    }
}

class CertificateHashExtractor {
    private final static String THUMBPRINT_ALGORITHM = "SHA-256";

    static Base64URL getHash(X509Certificate certificate) throws CertificateEncodingException, NoSuchAlgorithmException {
        return Base64URL.encode(MessageDigest.getInstance(THUMBPRINT_ALGORITHM).digest(certificate.getEncoded()));
    }
}
