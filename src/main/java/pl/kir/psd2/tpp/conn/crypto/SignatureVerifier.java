package pl.kir.psd2.tpp.conn.crypto;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.JWSVerifier;
import com.nimbusds.jose.crypto.RSASSAVerifier;
import com.nimbusds.jose.util.Base64URL;
import com.nimbusds.jwt.SignedJWT;
import org.junit.Assert;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPublicKey;
import java.text.ParseException;
import java.util.Optional;

public class SignatureVerifier {
    private Proxy proxy = null;

    public boolean verifySignature(String jws, String payload) {
        try {
            String[] tokenParts = jws.split("\\.");

            SignedJWT signedJWT = new SignedJWT(
                    new Base64URL(tokenParts[0]),
                    Base64URL.encode(payload),
                    new Base64URL(tokenParts[2])
            );

            X509Certificate aspspCertificate = getAspspCertificateFromX5CorX5U(signedJWT);
            JWSVerifier verifier = new RSASSAVerifier((RSAPublicKey) aspspCertificate.getPublicKey());
            Assert.assertTrue("Verified JWS signature is incorrect", signedJWT.verify(verifier));

            String jwsKid = getKid(signedJWT.getHeader());
            String aspspCertificateKid = CertificateKidExtractor.getKid(aspspCertificate);
            Assert.assertEquals("KeyID from jws not equal to keyID in certificate", jwsKid, aspspCertificateKid);

            Base64URL jwsHash = getHash(signedJWT.getHeader());
            Base64URL aspspCertificateHash = CertificateHashExtractor.getHash(aspspCertificate);
            Assert.assertEquals("Hash from jws not equal to certificate hash", jwsHash, aspspCertificateHash);
            return true;
        } catch(IOException | CertificateException | JOSEException | NoSuchAlgorithmException | ParseException | KeyManagementException e) {
            e.printStackTrace();
            return false;
        }
    }

    private X509Certificate getAspspCertificateFromX5CorX5U(SignedJWT signedJWT) throws IOException, CertificateException, KeyManagementException, NoSuchAlgorithmException {
        try (InputStream is = signedJWT.getHeader().getX509CertURL() != null ?
                downloadCertificate(signedJWT.getHeader().getX509CertURL().toURL()) :
                new ByteArrayInputStream(signedJWT.getHeader().getX509CertChain().get(0).decode())) {
            CertificateFactory fact = CertificateFactory.getInstance("X.509");
            return (X509Certificate) fact.generateCertificate(is);
        }
    }

    private Base64URL getHash(JWSHeader jwsHeader) {
        return Optional.ofNullable(jwsHeader.getX509CertSHA256Thumbprint())
                .orElseThrow(() -> new IllegalArgumentException("The X.509 certificate hash not present in signature"));
    }

    private String getKid(JWSHeader jwsHeader) {
        return Optional.ofNullable(jwsHeader.getKeyID())
                .orElseThrow(() -> new IllegalArgumentException("The X.509 certificate kid not present in signature"));
    }

    private InputStream downloadCertificate(URL url) throws NoSuchAlgorithmException, KeyManagementException, IOException {
        TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
            public @Override void checkClientTrusted(X509Certificate[] chain, String authType) {}
            public @Override void checkServerTrusted(X509Certificate[] chain, String authType) {}
            public @Override X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[]{};
            }
        }};

        SSLContext sc = SSLContext.getInstance("SSL");
        sc.init(null, trustAllCerts, new SecureRandom());

        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        HttpsURLConnection.setDefaultHostnameVerifier((hostname, session) -> true);
        HttpURLConnection conn = (HttpURLConnection) (proxy == null ? url.openConnection() : url.openConnection(proxy));
        conn.setRequestMethod("GET");
        int responseCode = conn.getResponseCode();

        if (responseCode != HttpURLConnection.HTTP_OK) {
            System.out.println(conn.getResponseMessage());
            throw new IOException("certificate download link returned " + responseCode);
        }

        return conn.getInputStream();
    }

    public void setProxy(Proxy proxy) {
        this.proxy = proxy;
    }

    public void setProxy(String proxyHost, int port) {
        this.proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(proxyHost, port));
    }

}
