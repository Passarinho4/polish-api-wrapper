/*
 * Polish API
 * Interface specification for services provided by third parties based on access to payment accounts. Prepared by the Polish Bank Association and its affiliates
 *
 * OpenAPI spec version: 2_1_1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package pl.kir.psd2.tpp.polishapi.model;

import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Klasa odpowiedzi na pytanie o status paczki płatności / Response data with status of bundle of payments
 */
@ApiModel(description = "Klasa odpowiedzi na pytanie o status paczki płatności / Response data with status of bundle of payments")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2019-11-22T23:26:29.768+01:00")
public class BundleResponse {
  @SerializedName("responseHeader")
  private ResponseHeader responseHeader = null;

  @SerializedName("bundleId")
  private String bundleId = null;

  @SerializedName("tppBundleId")
  private String tppBundleId = null;

  /**
   * Status paczki przelewów / Bundle of payments status
   */
  @JsonAdapter(BundleStatusEnum.Adapter.class)
  public enum BundleStatusEnum {
    INPROGRESS("inProgress"),
    
    CANCELLED("cancelled"),
    
    DONE("done"),
    
    PARTIALLYDONE("partiallyDone");

    private String value;

    BundleStatusEnum(String value) {
      this.value = value;
    }

    public String getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }

    public static BundleStatusEnum fromValue(String text) {
      for (BundleStatusEnum b : BundleStatusEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }

    public static class Adapter extends TypeAdapter<BundleStatusEnum> {
      @Override
      public void write(final JsonWriter jsonWriter, final BundleStatusEnum enumeration) throws IOException {
        jsonWriter.value(enumeration.getValue());
      }

      @Override
      public BundleStatusEnum read(final JsonReader jsonReader) throws IOException {
        String value = jsonReader.nextString();
        return BundleStatusEnum.fromValue(String.valueOf(value));
      }
    }
  }

  @SerializedName("bundleStatus")
  private BundleStatusEnum bundleStatus = null;

  @SerializedName("bundleDetailedStatus")
  private String bundleDetailedStatus = null;

  @SerializedName("payments")
  private List<PaymentInfo> payments = null;

  public BundleResponse responseHeader(ResponseHeader responseHeader) {
    this.responseHeader = responseHeader;
    return this;
  }

   /**
   * Get responseHeader
   * @return responseHeader
  **/
  @ApiModelProperty(required = true, value = "")
  public ResponseHeader getResponseHeader() {
    return responseHeader;
  }

  public void setResponseHeader(ResponseHeader responseHeader) {
    this.responseHeader = responseHeader;
  }

  public BundleResponse bundleId(String bundleId) {
    this.bundleId = bundleId;
    return this;
  }

   /**
   * Identyfikator paczki przelewów / Bundle of payments identifier
   * @return bundleId
  **/
  @ApiModelProperty(required = true, value = "Identyfikator paczki przelewów / Bundle of payments identifier")
  public String getBundleId() {
    return bundleId;
  }

  public void setBundleId(String bundleId) {
    this.bundleId = bundleId;
  }

  public BundleResponse tppBundleId(String tppBundleId) {
    this.tppBundleId = tppBundleId;
    return this;
  }

   /**
   * Identyfikator paczki przelewów nadany przez TPP. / Bundle of payments identifier set by TPP.
   * @return tppBundleId
  **/
  @ApiModelProperty(required = true, value = "Identyfikator paczki przelewów nadany przez TPP. / Bundle of payments identifier set by TPP.")
  public String getTppBundleId() {
    return tppBundleId;
  }

  public void setTppBundleId(String tppBundleId) {
    this.tppBundleId = tppBundleId;
  }

  public BundleResponse bundleStatus(BundleStatusEnum bundleStatus) {
    this.bundleStatus = bundleStatus;
    return this;
  }

   /**
   * Status paczki przelewów / Bundle of payments status
   * @return bundleStatus
  **/
  @ApiModelProperty(value = "Status paczki przelewów / Bundle of payments status")
  public BundleStatusEnum getBundleStatus() {
    return bundleStatus;
  }

  public void setBundleStatus(BundleStatusEnum bundleStatus) {
    this.bundleStatus = bundleStatus;
  }

  public BundleResponse bundleDetailedStatus(String bundleDetailedStatus) {
    this.bundleDetailedStatus = bundleDetailedStatus;
    return this;
  }

   /**
   * Szczegółowy status paczki przelewów / Bundle of payments detailed status
   * @return bundleDetailedStatus
  **/
  @ApiModelProperty(value = "Szczegółowy status paczki przelewów / Bundle of payments detailed status")
  public String getBundleDetailedStatus() {
    return bundleDetailedStatus;
  }

  public void setBundleDetailedStatus(String bundleDetailedStatus) {
    this.bundleDetailedStatus = bundleDetailedStatus;
  }

  public BundleResponse payments(List<PaymentInfo> payments) {
    this.payments = payments;
    return this;
  }

  public BundleResponse addPaymentsItem(PaymentInfo paymentsItem) {
    if (this.payments == null) {
      this.payments = new ArrayList<>();
    }
    this.payments.add(paymentsItem);
    return this;
  }

   /**
   * Get payments
   * @return payments
  **/
  @ApiModelProperty(value = "")
  public List<PaymentInfo> getPayments() {
    return payments;
  }

  public void setPayments(List<PaymentInfo> payments) {
    this.payments = payments;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    BundleResponse bundleResponse = (BundleResponse) o;
    return Objects.equals(this.responseHeader, bundleResponse.responseHeader) &&
        Objects.equals(this.bundleId, bundleResponse.bundleId) &&
        Objects.equals(this.tppBundleId, bundleResponse.tppBundleId) &&
        Objects.equals(this.bundleStatus, bundleResponse.bundleStatus) &&
        Objects.equals(this.bundleDetailedStatus, bundleResponse.bundleDetailedStatus) &&
        Objects.equals(this.payments, bundleResponse.payments);
  }

  @Override
  public int hashCode() {
    return Objects.hash(responseHeader, bundleId, tppBundleId, bundleStatus, bundleDetailedStatus, payments);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class BundleResponse {\n");

    sb.append("    responseHeader: ").append(toIndentedString(responseHeader)).append("\n");
    sb.append("    bundleId: ").append(toIndentedString(bundleId)).append("\n");
    sb.append("    tppBundleId: ").append(toIndentedString(tppBundleId)).append("\n");
    sb.append("    bundleStatus: ").append(toIndentedString(bundleStatus)).append("\n");
    sb.append("    bundleDetailedStatus: ").append(toIndentedString(bundleDetailedStatus)).append("\n");
    sb.append("    payments: ").append(toIndentedString(payments)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

