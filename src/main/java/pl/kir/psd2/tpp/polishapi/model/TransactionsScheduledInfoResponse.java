/*
 * Polish API
 * Interface specification for services provided by third parties based on access to payment accounts. Prepared by the Polish Bank Association and its affiliates
 *
 * OpenAPI spec version: 2_1_1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package pl.kir.psd2.tpp.polishapi.model;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Klasa odpowiedzi zawierająca listę transakcji scheduled / Scheduled Transaction Information Response Class
 */
@ApiModel(description = "Klasa odpowiedzi zawierająca listę transakcji scheduled / Scheduled Transaction Information Response Class")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2019-11-22T23:26:29.768+01:00")
public class TransactionsScheduledInfoResponse {
  @SerializedName("responseHeader")
  private ResponseHeader responseHeader = null;

  @SerializedName("transactions")
  private List<TransactionScheduledInfo> transactions = null;

  @SerializedName("pageInfo")
  private PageInfo pageInfo = null;

  public TransactionsScheduledInfoResponse responseHeader(ResponseHeader responseHeader) {
    this.responseHeader = responseHeader;
    return this;
  }

   /**
   * Get responseHeader
   * @return responseHeader
  **/
  @ApiModelProperty(required = true, value = "")
  public ResponseHeader getResponseHeader() {
    return responseHeader;
  }

  public void setResponseHeader(ResponseHeader responseHeader) {
    this.responseHeader = responseHeader;
  }

  public TransactionsScheduledInfoResponse transactions(List<TransactionScheduledInfo> transactions) {
    this.transactions = transactions;
    return this;
  }

  public TransactionsScheduledInfoResponse addTransactionsItem(TransactionScheduledInfo transactionsItem) {
    if (this.transactions == null) {
      this.transactions = new ArrayList<>();
    }
    this.transactions.add(transactionsItem);
    return this;
  }

   /**
   * Get transactions
   * @return transactions
  **/
  @ApiModelProperty(value = "")
  public List<TransactionScheduledInfo> getTransactions() {
    return transactions;
  }

  public void setTransactions(List<TransactionScheduledInfo> transactions) {
    this.transactions = transactions;
  }

  public TransactionsScheduledInfoResponse pageInfo(PageInfo pageInfo) {
    this.pageInfo = pageInfo;
    return this;
  }

   /**
   * Get pageInfo
   * @return pageInfo
  **/
  @ApiModelProperty(value = "")
  public PageInfo getPageInfo() {
    return pageInfo;
  }

  public void setPageInfo(PageInfo pageInfo) {
    this.pageInfo = pageInfo;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TransactionsScheduledInfoResponse transactionsScheduledInfoResponse = (TransactionsScheduledInfoResponse) o;
    return Objects.equals(this.responseHeader, transactionsScheduledInfoResponse.responseHeader) &&
        Objects.equals(this.transactions, transactionsScheduledInfoResponse.transactions) &&
        Objects.equals(this.pageInfo, transactionsScheduledInfoResponse.pageInfo);
  }

  @Override
  public int hashCode() {
    return Objects.hash(responseHeader, transactions, pageInfo);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TransactionsScheduledInfoResponse {\n");

    sb.append("    responseHeader: ").append(toIndentedString(responseHeader)).append("\n");
    sb.append("    transactions: ").append(toIndentedString(transactions)).append("\n");
    sb.append("    pageInfo: ").append(toIndentedString(pageInfo)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

