/*
 * Polish API
 * Interface specification for services provided by third parties based on access to payment accounts. Prepared by the Polish Bank Association and its affiliates
 *
 * OpenAPI spec version: 2_1_1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package pl.kir.psd2.tpp.polishapi.api;

import com.google.gson.reflect.TypeToken;
import pl.kir.psd2.tpp.polishapi.*;
import pl.kir.psd2.tpp.polishapi.model.*;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AsApi {
    private ApiClient apiClient;

    public AsApi() {
        this(Configuration.getDefaultApiClient());
    }

    public AsApi(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    public ApiClient getApiClient() {
        return apiClient;
    }

    public void setApiClient(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    /**
     * Build call for authorize
     * @param acceptEncoding Gzip, deflate (required)
     * @param acceptLanguage Prefered language of response (required)
     * @param acceptCharset UTF-8 (required)
     * @param X_JWS_SIGNATURE Detached JWS signature of the body of the payload (required)
     * @param X_REQUEST_ID Identyfikator żądania w formacie UUID (Wariant 1, Wersja 1), zgodnym ze standardem RFC 4122, nadawany przez TPP. Wartość musi być zgodna z parametrem requestId przekazywanym w ciele każdego żądania. / Request identifier using UUID format (Variant 1, Version 1), described in RFC 4122 standard, set by TPP. Value of this header must be the same as for the requestId param passed inside request payload. (required)
     * @param authorizeRequest Data for OAuth2 Authorization Code Request (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call authorizeCall(String acceptEncoding, String acceptLanguage, String acceptCharset, String X_JWS_SIGNATURE, String X_REQUEST_ID, AuthorizeRequest authorizeRequest, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = authorizeRequest;

        // create path and map variables
        String localVarPath = "/v2_1_1.1/auth/v2_1_1.1/authorize";

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();
        if (acceptEncoding != null)
        localVarHeaderParams.put("Accept-Encoding", apiClient.parameterToString(acceptEncoding));
        if (acceptLanguage != null)
        localVarHeaderParams.put("Accept-Language", apiClient.parameterToString(acceptLanguage));
        if (acceptCharset != null)
        localVarHeaderParams.put("Accept-Charset", apiClient.parameterToString(acceptCharset));
        if (X_JWS_SIGNATURE != null)
        localVarHeaderParams.put("X-JWS-SIGNATURE", apiClient.parameterToString(X_JWS_SIGNATURE));
        if (X_REQUEST_ID != null)
        localVarHeaderParams.put("X-REQUEST-ID", apiClient.parameterToString(X_REQUEST_ID));

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] {  };
        return apiClient.buildCall(localVarPath, "POST", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call authorizeValidateBeforeCall(String acceptEncoding, String acceptLanguage, String acceptCharset, String X_JWS_SIGNATURE, String X_REQUEST_ID, AuthorizeRequest authorizeRequest, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {

        // verify the required parameter 'acceptEncoding' is set
        if (acceptEncoding == null) {
            throw new ApiException("Missing the required parameter 'acceptEncoding' when calling authorize(Async)");
        }

        // verify the required parameter 'acceptLanguage' is set
        if (acceptLanguage == null) {
            throw new ApiException("Missing the required parameter 'acceptLanguage' when calling authorize(Async)");
        }

        // verify the required parameter 'acceptCharset' is set
        if (acceptCharset == null) {
            throw new ApiException("Missing the required parameter 'acceptCharset' when calling authorize(Async)");
        }

        // verify the required parameter 'X_JWS_SIGNATURE' is set
        if (X_JWS_SIGNATURE == null) {
            throw new ApiException("Missing the required parameter 'X_JWS_SIGNATURE' when calling authorize(Async)");
        }

        // verify the required parameter 'X_REQUEST_ID' is set
        if (X_REQUEST_ID == null) {
            throw new ApiException("Missing the required parameter 'X_REQUEST_ID' when calling authorize(Async)");
        }

        // verify the required parameter 'authorizeRequest' is set
        if (authorizeRequest == null) {
            throw new ApiException("Missing the required parameter 'authorizeRequest' when calling authorize(Async)");
        }


        com.squareup.okhttp.Call call = authorizeCall(acceptEncoding, acceptLanguage, acceptCharset, X_JWS_SIGNATURE, X_REQUEST_ID, authorizeRequest, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Requests OAuth2 authorization code
     * Requests OAuth2 authorization code
     * @param acceptEncoding Gzip, deflate (required)
     * @param acceptLanguage Prefered language of response (required)
     * @param acceptCharset UTF-8 (required)
     * @param X_JWS_SIGNATURE Detached JWS signature of the body of the payload (required)
     * @param X_REQUEST_ID Identyfikator żądania w formacie UUID (Wariant 1, Wersja 1), zgodnym ze standardem RFC 4122, nadawany przez TPP. Wartość musi być zgodna z parametrem requestId przekazywanym w ciele każdego żądania. / Request identifier using UUID format (Variant 1, Version 1), described in RFC 4122 standard, set by TPP. Value of this header must be the same as for the requestId param passed inside request payload. (required)
     * @param authorizeRequest Data for OAuth2 Authorization Code Request (required)
     * @return AuthorizeResponse
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public AuthorizeResponse authorize(String acceptEncoding, String acceptLanguage, String acceptCharset, String X_JWS_SIGNATURE, String X_REQUEST_ID, AuthorizeRequest authorizeRequest) throws ApiException {
        ApiResponse<AuthorizeResponse> resp = authorizeWithHttpInfo(acceptEncoding, acceptLanguage, acceptCharset, X_JWS_SIGNATURE, X_REQUEST_ID, authorizeRequest);
        return resp.getData();
    }

    /**
     * Requests OAuth2 authorization code
     * Requests OAuth2 authorization code
     * @param acceptEncoding Gzip, deflate (required)
     * @param acceptLanguage Prefered language of response (required)
     * @param acceptCharset UTF-8 (required)
     * @param X_JWS_SIGNATURE Detached JWS signature of the body of the payload (required)
     * @param X_REQUEST_ID Identyfikator żądania w formacie UUID (Wariant 1, Wersja 1), zgodnym ze standardem RFC 4122, nadawany przez TPP. Wartość musi być zgodna z parametrem requestId przekazywanym w ciele każdego żądania. / Request identifier using UUID format (Variant 1, Version 1), described in RFC 4122 standard, set by TPP. Value of this header must be the same as for the requestId param passed inside request payload. (required)
     * @param authorizeRequest Data for OAuth2 Authorization Code Request (required)
     * @return ApiResponse&lt;AuthorizeResponse&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<AuthorizeResponse> authorizeWithHttpInfo(String acceptEncoding, String acceptLanguage, String acceptCharset, String X_JWS_SIGNATURE, String X_REQUEST_ID, AuthorizeRequest authorizeRequest) throws ApiException {
        com.squareup.okhttp.Call call = authorizeValidateBeforeCall(acceptEncoding, acceptLanguage, acceptCharset, X_JWS_SIGNATURE, X_REQUEST_ID, authorizeRequest, null, null);
        Type localVarReturnType = new TypeToken<AuthorizeResponse>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Requests OAuth2 authorization code (asynchronously)
     * Requests OAuth2 authorization code
     * @param acceptEncoding Gzip, deflate (required)
     * @param acceptLanguage Prefered language of response (required)
     * @param acceptCharset UTF-8 (required)
     * @param X_JWS_SIGNATURE Detached JWS signature of the body of the payload (required)
     * @param X_REQUEST_ID Identyfikator żądania w formacie UUID (Wariant 1, Wersja 1), zgodnym ze standardem RFC 4122, nadawany przez TPP. Wartość musi być zgodna z parametrem requestId przekazywanym w ciele każdego żądania. / Request identifier using UUID format (Variant 1, Version 1), described in RFC 4122 standard, set by TPP. Value of this header must be the same as for the requestId param passed inside request payload. (required)
     * @param authorizeRequest Data for OAuth2 Authorization Code Request (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call authorizeAsync(String acceptEncoding, String acceptLanguage, String acceptCharset, String X_JWS_SIGNATURE, String X_REQUEST_ID, AuthorizeRequest authorizeRequest, final ApiCallback<AuthorizeResponse> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = authorizeValidateBeforeCall(acceptEncoding, acceptLanguage, acceptCharset, X_JWS_SIGNATURE, X_REQUEST_ID, authorizeRequest, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<AuthorizeResponse>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for authorizeExt
     * @param acceptEncoding Gzip, deflate (required)
     * @param acceptLanguage Prefered language of response (required)
     * @param acceptCharset UTF-8 (required)
     * @param X_JWS_SIGNATURE Detached JWS signature of the body of the payload (required)
     * @param X_REQUEST_ID Identyfikator żądania w formacie UUID (Wariant 1, Wersja 1), zgodnym ze standardem RFC 4122, nadawany przez TPP. Wartość musi być zgodna z parametrem requestId przekazywanym w ciele każdego żądania. / Request identifier using UUID format (Variant 1, Version 1), described in RFC 4122 standard, set by TPP. Value of this header must be the same as for the requestId param passed inside request payload. (required)
     * @param eatCodeRequest Data for OAuth2 Authorization Code Request extended for EAT based authentication and callback response (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call authorizeExtCall(String acceptEncoding, String acceptLanguage, String acceptCharset, String X_JWS_SIGNATURE, String X_REQUEST_ID, EatCodeRequest eatCodeRequest, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = eatCodeRequest;

        // create path and map variables
        String localVarPath = "/v2_1_1.1/auth/v2_1_1.1/authorizeExt";

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();
        if (acceptEncoding != null)
        localVarHeaderParams.put("Accept-Encoding", apiClient.parameterToString(acceptEncoding));
        if (acceptLanguage != null)
        localVarHeaderParams.put("Accept-Language", apiClient.parameterToString(acceptLanguage));
        if (acceptCharset != null)
        localVarHeaderParams.put("Accept-Charset", apiClient.parameterToString(acceptCharset));
        if (X_JWS_SIGNATURE != null)
        localVarHeaderParams.put("X-JWS-SIGNATURE", apiClient.parameterToString(X_JWS_SIGNATURE));
        if (X_REQUEST_ID != null)
        localVarHeaderParams.put("X-REQUEST-ID", apiClient.parameterToString(X_REQUEST_ID));

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] {  };
        return apiClient.buildCall(localVarPath, "POST", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call authorizeExtValidateBeforeCall(String acceptEncoding, String acceptLanguage, String acceptCharset, String X_JWS_SIGNATURE, String X_REQUEST_ID, EatCodeRequest eatCodeRequest, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {

        // verify the required parameter 'acceptEncoding' is set
        if (acceptEncoding == null) {
            throw new ApiException("Missing the required parameter 'acceptEncoding' when calling authorizeExt(Async)");
        }

        // verify the required parameter 'acceptLanguage' is set
        if (acceptLanguage == null) {
            throw new ApiException("Missing the required parameter 'acceptLanguage' when calling authorizeExt(Async)");
        }

        // verify the required parameter 'acceptCharset' is set
        if (acceptCharset == null) {
            throw new ApiException("Missing the required parameter 'acceptCharset' when calling authorizeExt(Async)");
        }

        // verify the required parameter 'X_JWS_SIGNATURE' is set
        if (X_JWS_SIGNATURE == null) {
            throw new ApiException("Missing the required parameter 'X_JWS_SIGNATURE' when calling authorizeExt(Async)");
        }

        // verify the required parameter 'X_REQUEST_ID' is set
        if (X_REQUEST_ID == null) {
            throw new ApiException("Missing the required parameter 'X_REQUEST_ID' when calling authorizeExt(Async)");
        }

        // verify the required parameter 'eatCodeRequest' is set
        if (eatCodeRequest == null) {
            throw new ApiException("Missing the required parameter 'eatCodeRequest' when calling authorizeExt(Async)");
        }


        com.squareup.okhttp.Call call = authorizeExtCall(acceptEncoding, acceptLanguage, acceptCharset, X_JWS_SIGNATURE, X_REQUEST_ID, eatCodeRequest, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Requests OAuth2 authorization code based on One-time authorization code issued by External Authorization Tool
     * Requests OAuth2 authorization code based One-time authorization code issued by External Authorization Tool. Authorization code will be delivered to TPP as callback request from ASPSP if PSU authentication is confirmed by EAT. Callback function must provide similar notification also in case of unsuccessful authentication or its abandonment.
     * @param acceptEncoding Gzip, deflate (required)
     * @param acceptLanguage Prefered language of response (required)
     * @param acceptCharset UTF-8 (required)
     * @param X_JWS_SIGNATURE Detached JWS signature of the body of the payload (required)
     * @param X_REQUEST_ID Identyfikator żądania w formacie UUID (Wariant 1, Wersja 1), zgodnym ze standardem RFC 4122, nadawany przez TPP. Wartość musi być zgodna z parametrem requestId przekazywanym w ciele każdego żądania. / Request identifier using UUID format (Variant 1, Version 1), described in RFC 4122 standard, set by TPP. Value of this header must be the same as for the requestId param passed inside request payload. (required)
     * @param eatCodeRequest Data for OAuth2 Authorization Code Request extended for EAT based authentication and callback response (required)
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public void authorizeExt(String acceptEncoding, String acceptLanguage, String acceptCharset, String X_JWS_SIGNATURE, String X_REQUEST_ID, EatCodeRequest eatCodeRequest) throws ApiException {
        authorizeExtWithHttpInfo(acceptEncoding, acceptLanguage, acceptCharset, X_JWS_SIGNATURE, X_REQUEST_ID, eatCodeRequest);
    }

    /**
     * Requests OAuth2 authorization code based on One-time authorization code issued by External Authorization Tool
     * Requests OAuth2 authorization code based One-time authorization code issued by External Authorization Tool. Authorization code will be delivered to TPP as callback request from ASPSP if PSU authentication is confirmed by EAT. Callback function must provide similar notification also in case of unsuccessful authentication or its abandonment.
     * @param acceptEncoding Gzip, deflate (required)
     * @param acceptLanguage Prefered language of response (required)
     * @param acceptCharset UTF-8 (required)
     * @param X_JWS_SIGNATURE Detached JWS signature of the body of the payload (required)
     * @param X_REQUEST_ID Identyfikator żądania w formacie UUID (Wariant 1, Wersja 1), zgodnym ze standardem RFC 4122, nadawany przez TPP. Wartość musi być zgodna z parametrem requestId przekazywanym w ciele każdego żądania. / Request identifier using UUID format (Variant 1, Version 1), described in RFC 4122 standard, set by TPP. Value of this header must be the same as for the requestId param passed inside request payload. (required)
     * @param eatCodeRequest Data for OAuth2 Authorization Code Request extended for EAT based authentication and callback response (required)
     * @return ApiResponse&lt;Void&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<Void> authorizeExtWithHttpInfo(String acceptEncoding, String acceptLanguage, String acceptCharset, String X_JWS_SIGNATURE, String X_REQUEST_ID, EatCodeRequest eatCodeRequest) throws ApiException {
        com.squareup.okhttp.Call call = authorizeExtValidateBeforeCall(acceptEncoding, acceptLanguage, acceptCharset, X_JWS_SIGNATURE, X_REQUEST_ID, eatCodeRequest, null, null);
        return apiClient.execute(call);
    }

    /**
     * Requests OAuth2 authorization code based on One-time authorization code issued by External Authorization Tool (asynchronously)
     * Requests OAuth2 authorization code based One-time authorization code issued by External Authorization Tool. Authorization code will be delivered to TPP as callback request from ASPSP if PSU authentication is confirmed by EAT. Callback function must provide similar notification also in case of unsuccessful authentication or its abandonment.
     * @param acceptEncoding Gzip, deflate (required)
     * @param acceptLanguage Prefered language of response (required)
     * @param acceptCharset UTF-8 (required)
     * @param X_JWS_SIGNATURE Detached JWS signature of the body of the payload (required)
     * @param X_REQUEST_ID Identyfikator żądania w formacie UUID (Wariant 1, Wersja 1), zgodnym ze standardem RFC 4122, nadawany przez TPP. Wartość musi być zgodna z parametrem requestId przekazywanym w ciele każdego żądania. / Request identifier using UUID format (Variant 1, Version 1), described in RFC 4122 standard, set by TPP. Value of this header must be the same as for the requestId param passed inside request payload. (required)
     * @param eatCodeRequest Data for OAuth2 Authorization Code Request extended for EAT based authentication and callback response (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call authorizeExtAsync(String acceptEncoding, String acceptLanguage, String acceptCharset, String X_JWS_SIGNATURE, String X_REQUEST_ID, EatCodeRequest eatCodeRequest, final ApiCallback<Void> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = authorizeExtValidateBeforeCall(acceptEncoding, acceptLanguage, acceptCharset, X_JWS_SIGNATURE, X_REQUEST_ID, eatCodeRequest, progressListener, progressRequestListener);
        apiClient.executeAsync(call, callback);
        return call;
    }
    /**
     * Build call for token
     * @param acceptEncoding Gzip, deflate (required)
     * @param acceptLanguage Prefered language of response (required)
     * @param acceptCharset UTF-8 (required)
     * @param X_JWS_SIGNATURE Detached JWS signature of the body of the payload (required)
     * @param X_REQUEST_ID Identyfikator żądania w formacie UUID (Wariant 1, Wersja 1), zgodnym ze standardem RFC 4122, nadawany przez TPP. Wartość musi być zgodna z parametrem requestId przekazywanym w ciele każdego żądania. / Request identifier using UUID format (Variant 1, Version 1), described in RFC 4122 standard, set by TPP. Value of this header must be the same as for the requestId param passed inside request payload. (required)
     * @param tokenRequest Data for OAuth2 Access Token Request (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call tokenCall(String acceptEncoding, String acceptLanguage, String acceptCharset, String X_JWS_SIGNATURE, String X_REQUEST_ID, TokenRequest tokenRequest, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = tokenRequest;

        // create path and map variables
        String localVarPath = "/v2_1_1.1/auth/v2_1_1.1/token";

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();
        if (acceptEncoding != null)
        localVarHeaderParams.put("Accept-Encoding", apiClient.parameterToString(acceptEncoding));
        if (acceptLanguage != null)
        localVarHeaderParams.put("Accept-Language", apiClient.parameterToString(acceptLanguage));
        if (acceptCharset != null)
        localVarHeaderParams.put("Accept-Charset", apiClient.parameterToString(acceptCharset));
        if (X_JWS_SIGNATURE != null)
        localVarHeaderParams.put("X-JWS-SIGNATURE", apiClient.parameterToString(X_JWS_SIGNATURE));
        if (X_REQUEST_ID != null)
        localVarHeaderParams.put("X-REQUEST-ID", apiClient.parameterToString(X_REQUEST_ID));

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] {  };
        return apiClient.buildCall(localVarPath, "POST", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call tokenValidateBeforeCall(String acceptEncoding, String acceptLanguage, String acceptCharset, String X_JWS_SIGNATURE, String X_REQUEST_ID, TokenRequest tokenRequest, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'acceptEncoding' is set
        if (acceptEncoding == null) {
            throw new ApiException("Missing the required parameter 'acceptEncoding' when calling token(Async)");
        }
        
        // verify the required parameter 'acceptLanguage' is set
        if (acceptLanguage == null) {
            throw new ApiException("Missing the required parameter 'acceptLanguage' when calling token(Async)");
        }
        
        // verify the required parameter 'acceptCharset' is set
        if (acceptCharset == null) {
            throw new ApiException("Missing the required parameter 'acceptCharset' when calling token(Async)");
        }
        
        // verify the required parameter 'X_JWS_SIGNATURE' is set
        if (X_JWS_SIGNATURE == null) {
            throw new ApiException("Missing the required parameter 'X_JWS_SIGNATURE' when calling token(Async)");
        }
        
        // verify the required parameter 'X_REQUEST_ID' is set
        if (X_REQUEST_ID == null) {
            throw new ApiException("Missing the required parameter 'X_REQUEST_ID' when calling token(Async)");
        }
        
        // verify the required parameter 'tokenRequest' is set
        if (tokenRequest == null) {
            throw new ApiException("Missing the required parameter 'tokenRequest' when calling token(Async)");
        }
        

        com.squareup.okhttp.Call call = tokenCall(acceptEncoding, acceptLanguage, acceptCharset, X_JWS_SIGNATURE, X_REQUEST_ID, tokenRequest, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Requests OAuth2 access token value
     * Requests OAuth2 access token value
     * @param acceptEncoding Gzip, deflate (required)
     * @param acceptLanguage Prefered language of response (required)
     * @param acceptCharset UTF-8 (required)
     * @param X_JWS_SIGNATURE Detached JWS signature of the body of the payload (required)
     * @param X_REQUEST_ID Identyfikator żądania w formacie UUID (Wariant 1, Wersja 1), zgodnym ze standardem RFC 4122, nadawany przez TPP. Wartość musi być zgodna z parametrem requestId przekazywanym w ciele każdego żądania. / Request identifier using UUID format (Variant 1, Version 1), described in RFC 4122 standard, set by TPP. Value of this header must be the same as for the requestId param passed inside request payload. (required)
     * @param tokenRequest Data for OAuth2 Access Token Request (required)
     * @return TokenResponse
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public TokenResponse token(String acceptEncoding, String acceptLanguage, String acceptCharset, String X_JWS_SIGNATURE, String X_REQUEST_ID, TokenRequest tokenRequest) throws ApiException {
        ApiResponse<TokenResponse> resp = tokenWithHttpInfo(acceptEncoding, acceptLanguage, acceptCharset, X_JWS_SIGNATURE, X_REQUEST_ID, tokenRequest);
        return resp.getData();
    }

    /**
     * Requests OAuth2 access token value
     * Requests OAuth2 access token value
     * @param acceptEncoding Gzip, deflate (required)
     * @param acceptLanguage Prefered language of response (required)
     * @param acceptCharset UTF-8 (required)
     * @param X_JWS_SIGNATURE Detached JWS signature of the body of the payload (required)
     * @param X_REQUEST_ID Identyfikator żądania w formacie UUID (Wariant 1, Wersja 1), zgodnym ze standardem RFC 4122, nadawany przez TPP. Wartość musi być zgodna z parametrem requestId przekazywanym w ciele każdego żądania. / Request identifier using UUID format (Variant 1, Version 1), described in RFC 4122 standard, set by TPP. Value of this header must be the same as for the requestId param passed inside request payload. (required)
     * @param tokenRequest Data for OAuth2 Access Token Request (required)
     * @return ApiResponse&lt;TokenResponse&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<TokenResponse> tokenWithHttpInfo(String acceptEncoding, String acceptLanguage, String acceptCharset, String X_JWS_SIGNATURE, String X_REQUEST_ID, TokenRequest tokenRequest) throws ApiException {
        com.squareup.okhttp.Call call = tokenValidateBeforeCall(acceptEncoding, acceptLanguage, acceptCharset, X_JWS_SIGNATURE, X_REQUEST_ID, tokenRequest, null, null);
        Type localVarReturnType = new TypeToken<TokenResponse>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Requests OAuth2 access token value (asynchronously)
     * Requests OAuth2 access token value
     * @param acceptEncoding Gzip, deflate (required)
     * @param acceptLanguage Prefered language of response (required)
     * @param acceptCharset UTF-8 (required)
     * @param X_JWS_SIGNATURE Detached JWS signature of the body of the payload (required)
     * @param X_REQUEST_ID Identyfikator żądania w formacie UUID (Wariant 1, Wersja 1), zgodnym ze standardem RFC 4122, nadawany przez TPP. Wartość musi być zgodna z parametrem requestId przekazywanym w ciele każdego żądania. / Request identifier using UUID format (Variant 1, Version 1), described in RFC 4122 standard, set by TPP. Value of this header must be the same as for the requestId param passed inside request payload. (required)
     * @param tokenRequest Data for OAuth2 Access Token Request (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call tokenAsync(String acceptEncoding, String acceptLanguage, String acceptCharset, String X_JWS_SIGNATURE, String X_REQUEST_ID, TokenRequest tokenRequest, final ApiCallback<TokenResponse> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = tokenValidateBeforeCall(acceptEncoding, acceptLanguage, acceptCharset, X_JWS_SIGNATURE, X_REQUEST_ID, tokenRequest, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<TokenResponse>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
}
