/*
 * Polish API
 * Interface specification for services provided by third parties based on access to payment accounts. Prepared by the Polish Bank Association and its affiliates
 *
 * OpenAPI spec version: 2_1_1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package pl.kir.psd2.tpp.polishapi.model;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

/**
 * Klasa zapytania o dostępne środki płatnicze na rachunku / Confirmation of Funds Request Class
 */
@ApiModel(description = "Klasa zapytania o dostępne środki płatnicze na rachunku / Confirmation of Funds Request Class")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2019-11-22T23:26:29.768+01:00")
public class ConfirmationOfFundsRequest {
  @SerializedName("requestHeader")
  private RequestHeaderWithoutToken requestHeader = null;

  @SerializedName("accountNumber")
  private String accountNumber = null;

  @SerializedName("amount")
  private String amount = null;

  @SerializedName("currency")
  private String currency = null;

  public ConfirmationOfFundsRequest requestHeader(RequestHeaderWithoutToken requestHeader) {
    this.requestHeader = requestHeader;
    return this;
  }

   /**
   * Get requestHeader
   * @return requestHeader
  **/
  @ApiModelProperty(required = true, value = "")
  public RequestHeaderWithoutToken getRequestHeader() {
    return requestHeader;
  }

  public void setRequestHeader(RequestHeaderWithoutToken requestHeader) {
    this.requestHeader = requestHeader;
  }

  public ConfirmationOfFundsRequest accountNumber(String accountNumber) {
    this.accountNumber = accountNumber;
    return this;
  }

   /**
   * Numer konta / Account number
   * @return accountNumber
  **/
  @ApiModelProperty(required = true, value = "Numer konta / Account number")
  public String getAccountNumber() {
    return accountNumber;
  }

  public void setAccountNumber(String accountNumber) {
    this.accountNumber = accountNumber;
  }

  public ConfirmationOfFundsRequest amount(String amount) {
    this.amount = amount;
    return this;
  }

   /**
   * Wielkość środków której dotyczy zaptanie / Amount of the transaction
   * @return amount
  **/
  @ApiModelProperty(required = true, value = "Wielkość środków której dotyczy zaptanie / Amount of the transaction")
  public String getAmount() {
    return amount;
  }

  public void setAmount(String amount) {
    this.amount = amount;
  }

  public ConfirmationOfFundsRequest currency(String currency) {
    this.currency = currency;
    return this;
  }

   /**
   * Kod ISO Waluty (waluta transakcji) / Currency of transaction (ISO)
   * @return currency
  **/
  @ApiModelProperty(required = true, value = "Kod ISO Waluty (waluta transakcji) / Currency of transaction (ISO)")
  public String getCurrency() {
    return currency;
  }

  public void setCurrency(String currency) {
    this.currency = currency;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ConfirmationOfFundsRequest confirmationOfFundsRequest = (ConfirmationOfFundsRequest) o;
    return Objects.equals(this.requestHeader, confirmationOfFundsRequest.requestHeader) &&
        Objects.equals(this.accountNumber, confirmationOfFundsRequest.accountNumber) &&
        Objects.equals(this.amount, confirmationOfFundsRequest.amount) &&
        Objects.equals(this.currency, confirmationOfFundsRequest.currency);
  }

  @Override
  public int hashCode() {
    return Objects.hash(requestHeader, accountNumber, amount, currency);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ConfirmationOfFundsRequest {\n");

    sb.append("    requestHeader: ").append(toIndentedString(requestHeader)).append("\n");
    sb.append("    accountNumber: ").append(toIndentedString(accountNumber)).append("\n");
    sb.append("    amount: ").append(toIndentedString(amount)).append("\n");
    sb.append("    currency: ").append(toIndentedString(currency)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

