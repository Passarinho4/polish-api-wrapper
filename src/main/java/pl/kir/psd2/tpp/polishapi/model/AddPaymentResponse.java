/*
 * Polish API
 * Interface specification for services provided by third parties based on access to payment accounts. Prepared by the Polish Bank Association and its affiliates
 *
 * OpenAPI spec version: 2_1_1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package pl.kir.psd2.tpp.polishapi.model;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

/**
 * Klasa odpowiedzi zlecenia płatności / Payment Add Response Class
 */
@ApiModel(description = "Klasa odpowiedzi zlecenia płatności / Payment Add Response Class")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2019-11-22T23:26:29.768+01:00")
public class AddPaymentResponse {
  @SerializedName("responseHeader")
  private ResponseHeader responseHeader = null;

  @SerializedName("paymentId")
  private String paymentId = null;

  @SerializedName("generalStatus")
  private PaymentStatus generalStatus = null;

  @SerializedName("detailedStatus")
  private String detailedStatus = null;

  public AddPaymentResponse responseHeader(ResponseHeader responseHeader) {
    this.responseHeader = responseHeader;
    return this;
  }

   /**
   * Get responseHeader
   * @return responseHeader
  **/
  @ApiModelProperty(required = true, value = "")
  public ResponseHeader getResponseHeader() {
    return responseHeader;
  }

  public void setResponseHeader(ResponseHeader responseHeader) {
    this.responseHeader = responseHeader;
  }

  public AddPaymentResponse paymentId(String paymentId) {
    this.paymentId = paymentId;
    return this;
  }

   /**
   * Identyfiaktor płatności / Payment ID
   * @return paymentId
  **/
  @ApiModelProperty(required = true, value = "Identyfiaktor płatności / Payment ID")
  public String getPaymentId() {
    return paymentId;
  }

  public void setPaymentId(String paymentId) {
    this.paymentId = paymentId;
  }

  public AddPaymentResponse generalStatus(PaymentStatus generalStatus) {
    this.generalStatus = generalStatus;
    return this;
  }

   /**
   * Get generalStatus
   * @return generalStatus
  **/
  @ApiModelProperty(required = true, value = "")
  public PaymentStatus getGeneralStatus() {
    return generalStatus;
  }

  public void setGeneralStatus(PaymentStatus generalStatus) {
    this.generalStatus = generalStatus;
  }

  public AddPaymentResponse detailedStatus(String detailedStatus) {
    this.detailedStatus = detailedStatus;
    return this;
  }

   /**
   * Status płatności / Detailed payment status
   * @return detailedStatus
  **/
  @ApiModelProperty(required = true, value = "Status płatności / Detailed payment status")
  public String getDetailedStatus() {
    return detailedStatus;
  }

  public void setDetailedStatus(String detailedStatus) {
    this.detailedStatus = detailedStatus;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AddPaymentResponse addPaymentResponse = (AddPaymentResponse) o;
    return Objects.equals(this.responseHeader, addPaymentResponse.responseHeader) &&
        Objects.equals(this.paymentId, addPaymentResponse.paymentId) &&
        Objects.equals(this.generalStatus, addPaymentResponse.generalStatus) &&
        Objects.equals(this.detailedStatus, addPaymentResponse.detailedStatus);
  }

  @Override
  public int hashCode() {
    return Objects.hash(responseHeader, paymentId, generalStatus, detailedStatus);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AddPaymentResponse {\n");

    sb.append("    responseHeader: ").append(toIndentedString(responseHeader)).append("\n");
    sb.append("    paymentId: ").append(toIndentedString(paymentId)).append("\n");
    sb.append("    generalStatus: ").append(toIndentedString(generalStatus)).append("\n");
    sb.append("    detailedStatus: ").append(toIndentedString(detailedStatus)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

